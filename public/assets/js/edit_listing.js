var QueryString = function () {
  // This function is anonymous, is executed immediately and 
  // the return value is assigned to QueryString!
  var query_string = {};
  var query = window.location.search.substring(1);
  var vars = query.split("&");
  for (var i=0;i<vars.length;i++) {
    var pair = vars[i].split("=");
        // If first entry with this name
    if (typeof query_string[pair[0]] === "undefined") {
      query_string[pair[0]] = decodeURIComponent(pair[1]);
        // If second entry with this name
    } else if (typeof query_string[pair[0]] === "string") {
      var arr = [ query_string[pair[0]],decodeURIComponent(pair[1]) ];
      query_string[pair[0]] = arr;
        // If third or later entry with this name
    } else {
      query_string[pair[0]].push(decodeURIComponent(pair[1]));
    }
  } 
    return query_string;
}();


var Listing_ID = $('#listing_id').attr('data-value');
var Completion = $('#listing_status').attr('data-value');
var low_home = $('#r_listing_low_home').attr('data-value');
var hi_home = $('#r_listing_hi_home').attr('data-value');
var other_home = $('#r_listing_other').attr('data-value');
var choose_low = $('#r_listing_choose_low').attr('data-value');
var choose_high = $('#r_listing_choose_high').attr('data-value');
var wants_neigh = ($('#wants_neigh').attr('data-value') === 'true');
var neighbourhood_h = ($('#neighbourhood_h').attr('data-value') === 'true');
var browser_name = $('#browser').attr('data-name');
var browser_version = $('#browser').attr('data-version');



if(QueryString.pos){
    var scroll_to = QueryString.pos;
} else {
    var scroll_to = null;
}

if(browser_name === 'Internet Explorer' && parseInt(browser_version) <= 9 ) {
    var no_drag = true;
} else {
    var no_drag = false;
}


$(function(){

	$(".multi").multiselect({
		selectedList: 1,
		header: "Select Choices",
		noneSelectedText: "Select Choices"
	});
	
	var checked = [];
	var total = [];
	
	$(".multi").on("multiselectclick", function(event, ui) {
		 checked = [];
		 total = [];
		 
		 //$(this).multiselect("widget").find(":checked").each(function(event, ui){
   		//		checked.push(ui.value);
		//});
		checked = $(this).val();
		
		if(!checked){
			checked = [0];
		}
		
		$(this).multiselect("widget").find(":checkbox").each(function(){
			total.push(ui.value);
		});
		 //console.log("Array: "+checked+" Array Length: "+checked.length+ " :: Checked Value: "+ui.value+" Checked Or No: "+ui.checked);
		 
		 if(ui.value == "9" && ui.checked == true || checked.length == 0 || (checked.length+1) == total.length){
			$(this).multiselect("uncheckAll");
			$(this).val('9');
			$(this).multiselect('refresh');
		 }else if(checked.indexOf('9') !== -1){
			$(this).multiselect("widget").find(":checkbox[value='9']").each(function(){
   				$(this).prop('checked', false);
			});
		  }
	});
	
	$(".multi").on("multiselectchange", function(event, ui) {
		 checked = [];
		 total = [];
		 
		 //$(this).multiselect("widget").find(":checked").each(function(event, ui){
   		//		checked.push(ui.value);
		//});
		checked = $(this).val();
		
		if(!checked){
			checked = [0];
		}
		
		$(this).multiselect("widget").find(":checkbox").each(function(){
			total.push(ui.value);
		});
		 //console.log("Array: "+checked+" Array Length: "+checked.length+ " :: Checked Value: "+ui.value+" Checked Or No: "+ui.checked);
		 
		 if(ui.value == "9" && ui.checked == true || checked.length == 0 || (checked.length+1) == total.length){
			$(this).multiselect("uncheckAll");
			$(this).val('9');
			$(this).multiselect('refresh');
		 }else if(checked.indexOf('9') !== -1){
			$(this).multiselect("widget").find(":checkbox[value='9']").each(function(){
   				$(this).prop('checked', false);
			});
		 }
	});
	
	$("#neighbourhood_w").on("multiselectchange", function(event, ui) {
		checked = [];
		total = [];
		
		checked = $(this).multiselect("getChecked").map(function(){
   					  return this.value;    
				  }).get();
				  
		if(!checked){
			checked = [0];
		}
		
		total = $(this).multiselect("widget");
				 
		if(ui.value == "none" && ui.checked == true || checked.length == 0 || (checked.length+1) == total.length){
			//$(this).multiselect("checkAll");
			$(this).val('none');
			$(this).multiselect('refresh');
		 }else if(checked.indexOf('none') !== -1){
			$(this).multiselect("widget").find(":checkbox[value='none']").each(function(){
   				$(this).prop('checked', false);
			});
		 }
		 if(ui.value == ''){
			$('label[for="neighbourhood_w').addClass("req_text").removeClass("req_text_done");
		}else{
			$('label[for="neighbourhood_w"]').addClass("req_text_done").removeClass("req_text");
		}
	});
	
	$("#neighbourhood_w").on("multiselectclick", function(event, ui) {
		checked = [];
		total = [];
		
		checked = $(this).multiselect("getChecked").map(function(){
   					  return this.value;    
				  }).get();
				  
		if(!checked){
			checked = 0;
		}
		
		total = $(this).multiselect("widget");
				 
		if(ui.value == "none" && ui.checked == true || checked.length == 0 || (checked.length+1) == total.length){
			//$(this).multiselect("checkAll");
			$(this).val('none');
			$(this).multiselect('refresh');
		 }else if(checked.indexOf('none') !== -1){
			$(this).multiselect("widget").find(":checkbox[value='none']").each(function(){
   				$(this).prop('checked', false);
			});
		 }
		 if(ui.value == ''){
			$('label[for="neighbourhood_w').addClass("req_text").removeClass("req_text_done");
		}else{
			$('label[for="neighbourhood_w"]').addClass("req_text_done").removeClass("req_text");
		}
	});
	
	$("#neighbourhood_w").multiselect({
		selectedList: 1,
		header: "Select Multiple",
		noneSelectedText: "Select Multiple"
	});
	
	$("#neighbourhood_h").on("multiselectchange", function(event, ui) {
		if(ui.value == ''){
			$('label[for="neighbourhood_h').addClass("req_text").removeClass("req_text_done");
		}else{
			$('label[for="neighbourhood_h"]').addClass("req_text_done").removeClass("req_text");
		}
	});
	
	$("#neighbourhood_h").on("multiselectclick", function(event, ui) {
		if(ui.value == ''){
			$('label[for="neighbourhood_h').addClass("req_text").removeClass("req_text_done");
		}else{
			$('label[for="neighbourhood_h"]').addClass("req_text_done").removeClass("req_text");
		}
	});
	
	$("#neighbourhood_h").multiselect({
		selectedList: 1,
		header: "<? echo $r_listing_sneigh; ?>",
		noneSelectedText: "<? echo $r_listing_sneigh; ?>"
	});
	
	//$('#neighbourhood_h').prop('disabled', 'disabled');
	//$("#neighbourhood_h").multiselect("disable");

	/* event: the original event object 
	ui.value: value of the checkbox 
	ui.text: text of the checkbox 
	ui.checked: whether or not the input was checked or unchecked (boolean) */ 
	
	$(".no_multi").multiselect({
		selectedList: 1,
		multiple: false
	});
	
	$(".drop_down_required").multiselect({
		selectedList: 1,
		classes: 'imp',
		multiple: false
	});
	
	$(".alert_d").multiselect({
		selectedList: 1,
		classes: 'mimp',
		multiple: false
	});
	
});

	$(function() {
		var availableTags = [
                    /*
		<?
		$get_school = mysql_query("SELECT DISTINCT School_Info FROM Client_Has WHERE School_Info NOT LIKE '0'");
		while ($row = mysql_fetch_array($get_school, MYSQL_NUM)) {
			printf("'%s',\n", $row[0]);
		}
		?>
                */
		];
		$( "#school" ).autocomplete({
			source: availableTags
		});
	});

    jQuery(function($){
   $("#pnumber_c").mask("(999) 999-9999");
   $("#cnumber_c").mask("(999) 999-9999");
   
   $("#wants_request").on("click", function(){
	   if($("#client_email_c").val() != ""){
			var client_email = $("#client_email_c").val();
	   }
		$('.pop_type').html("Send Request");
		$('.pop_content').html("Please enter your client's email address. They will receive a link to fill out their wants section.<br><br>Email: <input name='req_email' type='text' class='' id='req_email' value='<? if($client_email_c) echo $client_email_c; ?>' placeholder='Email Address'  /><br><span id='r_error' style='color:red;'></span><br><button class='buttons pop_button' id='submit_request' type='button'>Send Request</button><br><button class='buttons pop_button' id='close_button' type='button'>Close</button>");
		$('#multi_pop').reveal({ 
			animation: 'fadeAndPop',
			animationspeed: 600,
			closeonbackgroundclick: true,
			dismissmodalclass: 'cl_btn'
		});
		if($("#req_email").val() == ""){
			$("#req_email").val(client_email)
		}
   });
});

if(neighbourhood_h){
    $(document).ready(function() {
	$('label[for="neighbourhood_h"]').addClass("req_text_done").removeClass("req_text");
    });
}

if(Listing_ID == 'NA5377587207'){
$(document).ready(function() {
	var next_letter;
	
	$("#submit_content").attr("disabled", true);
	$("#scroll_to_next").attr("disabled", true);
	
	
	//document.onmousemove = function(e){
	//	var x = e.pageX;
	//	var y = e.pageY;
	//	$("#coords").html("left: "+x+" top: "+y);
	//};
	
	function typeText(placement, text_value, speed){
		var txt = text_value;
		var txt_content = txt.split("");
		var i = 0;
		
		function start_type(){
			if(i<txt_content.length){		
				$("#"+placement).val($("#"+placement).val()+txt_content[i]);
				i++;
			};
		};
		
		var next_letter = setInterval(start_type,speed);	
	};
	
	$("#tut_div").fadeIn(1500);
	
	$("#tut_continue").on("click", function(){
		$("#tut_cursor").fadeIn();
		$("#tut_div").fadeOut(500, function(){
			$("#tut_div").css("top", "290px");
			$("#tut_div").css("right", "650px");
			$("#tut_div_content").html("<br><br><br>Let's start with the client's first and last name, along with their email address.");
			$("#tut_div").fadeIn(500);
			//mouse_move_select("fname_c", "-", 0, "+", 0);
			mouse_move("fname_c", "John");
			setTimeout(function(){
				//mouse_move_select("lname_c", "-", 0, "+", 0);
				mouse_move("lname_c", "Smith");
				setTimeout(function(){
					//mouse_move_select("client_email_c", "-", 0, "+", 0);
					mouse_move("client_email_c", "tut@or.ial");
					setTimeout(function(){
						$("#client_email_c").trigger("change");
					}, 1500);
				}, 1750);
			}, 2500);
			setTimeout(function(){
				$("#tut_div").fadeOut(500, function(){
					$("#tut_div").css("right", "-100px");
					$("#tut_div_content").html('<br>With those complete, we click on the "Save Now" button.<br><br><button type="button" id="tut_next" name="1" class="fin_imp_btn">Next</button><br><button type="button" id="tut_close" class="fin_imp_btn">Cancel Tutorial</button>');
					$("#tut_div").fadeIn(500);
				});
			}, 7500);
		});
	});
	
	function mouse_move(name, text_func){
		var left = $("#"+name).offset().left - $(window).scrollLeft();//form_location.left;
		//console.log(form_location);
		//console.log($("#"+name).offset().left - $(window).scrollLeft());
		//pos_left = $(document).find("#"+name).offset().left-270;
		//pos_top = $(document).find("#"+name).offset().top-70;
		
		pos_left = left;//+left_var; //$(document).find("#"+name).offset().left;
		pos_top = $(document).find("#"+name).offset().top;
			$("#tut_cursor").animate({
				'left': pos_left,
				'top': pos_top
			}, 1000, function() {
				$("#tut_cursor_image").attr("src", "images/tutorial/cursor_show.gif");
				setTimeout(function(){
					$("#tut_cursor_image").attr("src", "images/tutorial/cursor.gif");
					if(text_func){
						text_function(name, text_func);
					}
				}, 1500);
			});
	}
	
	function mouse_move_select(name, oper_1, offset_x, oper_2, offset_y){
		if(oper_1 == "-"){
			//pos_left = $(document).find("#"+name).offset().left-offset_x;
			pos_left = $("#"+name).offset().left - $(window).scrollLeft()-offset_x;
		}else{
			//pos_left = $(document).find("#"+name).offset().left+offset_x;
			pos_left = $("#"+name).offset().left - $(window).scrollLeft()+offset_x;
		}
		if(oper_2 == "-"){
			pos_top = $(document).find("#"+name).offset().top-offset_y;
		}else{
			pos_top = $(document).find("#"+name).offset().top+offset_y;
		}
		//console.log(pos_left, pos_top);
			$("#tut_cursor").animate({
				'left': pos_left,
				'top': pos_top
			}, 1000);
	}
	
	function text_function(name, text_func){
		$("#"+name).focus();
		typeText(name, text_func, 200);
		clearInterval(next_letter);
	}
	
	function win_top(){
		return $(window).scrollTop();
	}
	
	$(document).delegate("#tut_next", "click", function(){
		var which_next = $(this).attr("name");
		
		if(which_next == 1){
			$("#tut_div").fadeOut(500, function(){
				$("#tut_div").css("top", "440px");
				$("#tut_div").css("right", "650px");
				$("#tut_div_content").html('<br><br>Now we move on to what your client has.<br><br><button type="button" id="tut_next" name="2" class="fin_imp_btn">Next</button><br><button type="button" id="tut_close" class="fin_imp_btn">Cancel Tutorial</button>');
			});
				mouse_move("submit_content");
				setTimeout(function(){
					$("#submit_content").trigger("click");
					setTimeout(function(){
						$("#tut_div").fadeIn(500);
						where_to = "has";
						$("#scroll_to_next").trigger("click");
					}, 2000);
				}, 2000);
		}
		
		if(which_next == 2){
			$("#tut_div").fadeOut(500, function(){
				$("#tut_div").css("top", "600px");
				$("#tut_div").css("right", "-100px");
				$("#tut_div_content").html('It`s best to confirm that the information xTradeHomes has gathered is accurate. Once completed, we check to see if required information is there. (<span style="font-weight:600; color: rgb(140, 5, 5); font-size: 12px;">Fields with red bolded headings</span>) We Need to fill out these required fields before we can move forward.<br><br><button type="button" id="tut_next" name="3" class="fin_imp_btn">Next</button><br><button type="button" id="tut_close" class="fin_imp_btn">Cancel Tutorial</button>');
				//$("#scroll_to_next").trigger("click");
			});
			setTimeout(function(){
				mouse_move("has_paddress");
				$("#tut_div").fadeIn(500);
			}, 500);
			setTimeout(function(){
				mouse_move("neighbourhood_h_label");
			}, 1800);
		}
		
		if(which_next == 3){
			$('#tut_next').attr("disabled", true);
			
			setTimeout(function(){
				$("#neighbourhood_h").multiselect("open");
			}, 400);
			setTimeout(function(){
				$("#neighbourhood_h").val("Arbutus");
				
				setTimeout(function(){
					$("#neighbourhood_h").multiselect('refresh');
					$("#neighbourhood_h").multiselect("close");
					$("#neighbourhood_h").trigger("change");
					$('label[for="neighbourhood_h"]').addClass("req_text_done").removeClass("req_text");
					//
					mouse_move_select("p_title_h_label", "-", 0, "+", 80);
					setTimeout(function(){
						$("#p_title_h").multiselect("open");
					}, 400);
					setTimeout(function(){
						select_item("p_title_h", "a");
							//
							mouse_move_select("basement_h_label", "-", 0, "+", 80);
							setTimeout(function(){
								$("#basement_h").multiselect("open");
							}, 400);
							setTimeout(function(){
								select_item("basement_h", "a");
								mouse_move_select("garage_h_label", "-", 0, "+", 80);
								setTimeout(function(){
									$("#garage_h").multiselect("open");
									
									$("#tut_div").fadeOut(500, function(){
										$("#tut_div").css("top", "800px");
										$("#tut_div").css("right", "650px");
										$("#tut_div_content").html('Only the fields in red are required, however, the more information you are able to fill in, the better the matching potential! Once you are satisfied with the "Has" area, click on "Save Now" to proceed to the next area.<br><br><button type="button" id="tut_next" name="4" class="fin_imp_btn">Next</button><br><button type="button" id="tut_close" class="fin_imp_btn">Cancel Tutorial</button>');
									});
									mouse_move_select("garage_h_label", "+", 200, "+", 80);
								}, 400);
								setTimeout(function(){
									$("#tut_div").fadeIn(500);
									select_item("garage_h", "a");
								}, 1100);
								//
							}, 1100);
							//
					}, 1100);
					//
				}, 400);
			}, 1100);
		}
		
		if(which_next == 4){
			mouse_move("submit_content");
			$("#tut_div").fadeOut(500, function(){
				$("#tut_div").css("top", "2075px");
				$("#tut_div").css("right", "-100px");
				$("#tut_div_content").html('The images associated with your properties are handled by your multiple listing service. If you wish to update photos, you must do so through the service you use.<br><br><button type="button" id="tut_next" name="5" class="fin_imp_btn">Next</button><br><button type="button" id="tut_close" class="fin_imp_btn">Cancel Tutorial</button>');
				$("#scroll_to_next").trigger("click");
			});
			setTimeout(function(){
				$("#submit_content").trigger("click");
				setTimeout(function(){
					scroll_to_next("image_info");
					$("#scroll_to_next").fadeOut();
					$("#tut_div").fadeIn(500);
				}, 2000);
			}, 2000);
		}
		
		if(which_next == 5){
			scroll_to_next("wants_info");
			$("#tut_div").fadeOut(500, function(){
				$("#tut_div").css("top", "2785px");
				$("#tut_div").css("right", "650px");
				$("#tut_div_content").html('In this section we will complete what your client "Wants". The more information you provide here, the greater the potential for matches!<br><br><button type="button" id="tut_next" name="6" class="fin_imp_btn">Next</button><br><button type="button" id="tut_close" class="fin_imp_btn">Cancel Tutorial</button>');
			});
			setTimeout(function(){
				$("#tut_div").fadeIn(500);
			}, 1500);
		}
		
		if(which_next == 6){
			$('#tut_next').attr("disabled", true);
			$("#wants_address").focus();
			mouse_move("wants_address", "Abbotsford");
			setTimeout(function(){
				$('#wants_address').autocomplete("search");
				mouse_move_select("wants_address", "+", 0, "+", 35);
				$("#tut_div").fadeOut(500);
				setTimeout(function(){
					$("#wants_address").trigger("change");
					$('#wants_address').autocomplete("search");
					var ac_id = $('#wants_address').data("ui-autocomplete").menu.element[0].id;
					$('#wants_address').val($('#'+ac_id).find("a").html());
					$('#wants_address').removeClass("ui-autocomplete-loading");
					item_selected = true;
					setTimeout(function(){
						$("#wants_address").blur();
						get_neighbourhood('neighbourhood_w','wants_address', 2, $("#wants_address").val());
						setTimeout(function(){
							$("#neighbourhood_w").multiselect("open");
							mouse_move_select("neighbourhood_w_label", "-", 0, "+", 120);
							select_item("neighbourhood_w", "Abbotsford/Highway 11", 1200);
							setTimeout(function(){
								$("#lprice_l_w").multiselect("open");
								mouse_move_select("lprice_l_w_label", "+", 75, "+", 110);
								select_item("lprice_l_w", "125000", 1200);
								setTimeout(function(){
									$("#lprice_h_w").multiselect("open");
									mouse_move_select("lprice_l_w_label", "+", 300, "+", 110);
									select_item("lprice_h_w", "125000", 1200);
									setTimeout(function(){
										$("#sq_ft_w").multiselect("open");
										mouse_move_select("sq_ft_w_label", "-", 0, "+", 110);
										select_item("sq_ft_w", "2400|3600", 1200);
											var _top = win_top()+20;
											$("#tut_div").css("top", _top+"px");
											$("#tut_div").css("right", "-100px");
											$("#tut_div_content").html('<br><br><br>Almost finished! We will complete all the required fields, and do our best to fill in other information our client has provided us with.');
										setTimeout(function(){
											setTimeout(function(){
												$("#tut_div").fadeIn(500);
											}, 1000);
											$("#storeys_w").multiselect("open");
											mouse_move_select("storeys_w_label", "-", 0, "+", 110);
											select_item("storeys_w", "a", 1200);
											setTimeout(function(){
												$("#bed_w").multiselect("open");
												mouse_move_select("bed_w_label", "-", 0, "+", 110);
												select_item("bed_w", "2", 1200);
												setTimeout(function(){
													$("#bath_w").multiselect("open");
													mouse_move_select("bath_w_label", "-", 0, "+", 110);
													select_item("bath_w", "2", 1200);
													setTimeout(function(){
														$("#p_title_w").multiselect("open");
														mouse_move_select("p_title_w_label", "-", 0, "+", 110);
														select_item("p_title_w", "a", 1200);
														setTimeout(function(){
															$("#basement_w").multiselect("open");
															mouse_move_select("basement_w_label", "-", 0, "+", 110);
															select_item("basement_w", "a", 1200);
															$("#tut_div").fadeOut(500, function(){
																$("#tut_div").css("top", "2850px");
																$("#tut_div").css("right", "-100px");
																$("#tut_div").css("height", "160px");
																$("#tut_div_content").html('<br>Now that we are finished, we can click on the "Save Now" button one more time to complete the listing.<br><br><button type="button" id="tut_next" name="7" class="fin_imp_btn">Next</button><br><button type="button" id="tut_close" class="fin_imp_btn">Cancel Tutorial</button>');
															});
															setTimeout(function(){
																$("#garage_w").multiselect("open");
																mouse_move_select("garage_w_label", "-", 0, "+", 110);
																select_item("garage_w", "a", 1200);
																setTimeout(function(){
																	$("#tut_div").fadeIn(500);
																}, 1000);
															}, 1500);
														}, 1500);
													}, 1500);
												}, 1500);
											}, 1500);
										}, 1500);
									}, 1500);
								}, 1500);
							}, 1500);
						}, 2500);
					}, 1000);
				}, 2000);
			}, 5000);
		}
		
		if(which_next == 7){
			$('#tut_next').attr("disabled", true);
			mouse_move("submit_content");
			$("#tut_div").fadeOut(500, function(){
				$("#tut_div").css("top", "375px");
				$("#tut_div").css("right", "auto");
				$("#tut_div").css("position", "fixed");
				$("#tut_div").css("margin", "auto 0");
				$("#tut_div").css("marginLeft", "-205px");
				$("#tut_div").css("left", "50%");
				$("#tut_div_content").html('<br>The listing is now complete! We are given the option to either go back to the Member`s Home (which is the My Listings button), Upgrade the Listing or Keep Editing. That`s it for the tutorial. Happy xTrading!<br><br><button type="button" id="tut_close" class="fin_imp_btn">Done</button>');
			});
			setTimeout(function(){
				$("#submit_content").trigger("click");
				setTimeout(function(){
					$('.pop_type').html("Listing Complete!");
					$('.pop_content').html("Your listing details are complete<br><br><button class='buttons pop_button' style='height: 57px;' id='Home' disabled>Go Back To<br>'My Listings'</button><br><button class='buttons pop_button' id='Upgrade' disabled>Upgrade Listing</button><button class='buttons pop_button' id='No' disabled>Keep Editing</button>");
					$('#multi_pop').reveal({ 
						animation: 'fadeAndPop',
						animationspeed: 600,
						closeonbackgroundclick: true,
					});
					$('#tut_next').attr("disabled", false);
					$("#tut_div").fadeIn(500);
				}, 3000);
			}, 2000);
			
		}
	});
	
	$("#tut_prev").on("click", function(){
		var which_prev = $(this).attr("name");
	});
	
	$(document).delegate("#tut_close", "click", function(){
		$("#tut_div").fadeOut(200, function(){
			window.location.assign('members_home.php');
		});
	});
	
	function select_item(from, what, close_time){
		if(!close_time){
			close_time = 600;
		}
		setTimeout(function(){
			$("#"+from).val(what);
		}, 400);						
		setTimeout(function(){
			$("#"+from).multiselect('refresh');
			$("#"+from).multiselect("close");
			$("#"+from).trigger("change");
		}, close_time);
	}
	
	function scroll_to_next(where){
		if(!where){
			where = where_to;
		}
		$('html, body').delay(250).animate({
			scrollTop: $('#'+where).offset().top
		}, 1000);
		
		$("#_message").hide();
	}
	
	function get_neighbourhood(load_in, sect, type, sval){
		if(!sval){
			var encoded = $("#"+sect).val();
		}else{
			var encoded = sval;
		}
		var Listing_ID = urlString("ID");
		$.post( "inc/location/get_neigh_inline.php", { location: encoded, lid: Listing_ID, type: type })
				.done(function( data ){
					//console.log( "Data Loaded: " + data );
					if(data == "none"){
						$("#"+load_in).html("<option value='none' selected disabled>None Available</option>");
						$("#"+load_in).multiselect({
								header: "None Available"
							});
						$("#"+load_in).prop('disabled', true);
						$("#"+load_in).multiselect("disable");
						$("#"+load_in).multiselect("refresh");
					}else{
						$("#"+load_in).html(data);
						setTimeout(function(){
							$("#"+load_in).multiselect({
								header: "Select Choice"
							});
							$("#"+load_in).multiselect("refresh");
							$("#"+load_in).prop('disabled', false);
							$("#"+load_in).multiselect("enable");
						},1000);
						if(load_in == "neighbourhood_w"){
							$('label[for="'+load_in+'"]').addClass("req_text_done").removeClass("req_text");
						}
					}
				});
	}
});
}