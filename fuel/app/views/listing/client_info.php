<div class="client_content" id="client_info">
  <div class="title_button">
    <div class="wants_sub_header title_box">Client Info <?php ($imported == 1 ? "<span style='font-size: 0.7em;'>ML#: ".$ml_num."</span>" : "");?>
      <a name="client" id="client"></a>
        <div class="attention_closed" title="<?php echo $attention_type_c; ?>" style="display:none;">
          <img src="images/attention.png" width="35" height="35" style="vertical-align: middle;" alt="Attention" class="attention_req">
        </div>
    </div>
  </div>
  <div class="client_content_sub <?php if($imported == 1) echo "no_edit"; ?> sub_area">
    <div class="up_to_date" title="This section is up to date." <?php echo ( !$new_client ? "style='display:block;'" : "style='display:none;'" ) ?>>
      Information Up To Date <img src="images/up-to-date.png" width="35" height="35" style="vertical-align: middle;" alt="Up to Date" class="uptodate">
    </div>
    <div class="attention" title="" <?php echo ( $new_client == 1 ? "style='display:block;'" : "style='display:none;'" ) ?>style='display:none;'></div><br>
    <div style="width: 700px;">
      This information is for your purposes only and therefore not mandatory. It will only be used for notifications sent to your email address. None of this information will be visible to the public.
    </div><br>
    <div class='wants_space'>
      <label for='fname_c' class=""><?=__('listing_fn'); ?></label>
    </div>
    <div class='wants_space'>
      <label for='lname_c' class=""><?=__('listing_ln'); ?></label>
    </div>
    <div class='wants_space'><?php echo $forgot_email; ?></div><br />
    <div class='wants_space'>
      <input name="<?=$fname_c['field'];?>" type="text" class="<?php echo $creq1; ?>" id="<?=$fname_c['field'];?>" value="<?=$fname_c['value'];?>" />
    </div>
    <div class='wants_space'>
      <input name="<?=$lname_c['field'];?>" type="text" class="<?php echo $creq2; ?>" id="<?=$lname_c['field'];?>" value="<?=$lname_c['value'];?>" />
    </div>
    <div class='wants_space'>
      <input name="<?=$client_email_c['field'];?>" type="text" class="input_boxs" id="<?=$client_email_c['field'];?>" value="<?=$client_email_c['value'];?>" />
    </div><br /><br />
    <div class='wants_space'>
    <?php echo $r_listing_pn; ?>
    </div>
    <div class='wants_space'><?php echo $r_listing_cn; ?></div><br />
    <div class='wants_space'>
      <input name="<?=$pnumber_c['field'];?>" type="text" class="<?php echo $creq3; ?>" id="<?=$pnumber_c['field'];?>" value="<?=$pnumber_c['value'];?>" />
    </div>
    <div class='wants_space'>
      <input name="<?=$cnumber_c['field'];?>" type="text" class="input_boxs" id="<?=$cnumber_c['field'];?>" value="<?=$cnumber_c['value'];?>" />
    </div>
    <a name="c"></a><br /><br /><br>
    <div class="submit_changes">
      <div class='section_complete multi_option' <?php echo ( !$new_client ? "style='display:block;'" : "style='display:none;'" ) ?>>
        Information Up To Date <img src="images/up-to-date.png" width="35" height="35" style="vertical-align: middle;" alt="Up to Date">
      </div>
      <div class='check_all multi_option' <?php echo ( $new_client == 1 ? "style='display:block;'" : "style='display:none;'" ) ?>></div>
      <div class='successful multi_option' style="display:none;">Update Successful</div>
    </div>
  </div>
</div>