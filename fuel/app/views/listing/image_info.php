<div class="client_content" id="image_info">
  <div class="<?php if($type_of_listing == 2) echo "no_title_button"; else echo "title_button"; ?>">
    <div class="wants_sub_header title_box"<?php //if($imported == 1){ echo " title='This listings photos were imported and cannot be changed on xTradeHomes.'"; }elseif($imported == 0 &&$type_of_listing == 2) echo " title='This is a buyer only listing. This area is locked.'"; ?>>Property Photo's <?php($imported == 1 ? "<span style='font-size: 0.7em;'>ML#: ".$ml_num."</span>" : "");?><a name="images" id="images"></a><div class="attention_closed" title="<?php //echo $attention_type_i; ?>" style="display:none;"><img src="images/attention.png" width="35" height="35" style="vertical-align: middle;" alt="Attention" class="attention_req"></div></div>
  </div>
  <div class="client_content_sub <?php //if($type_of_listing == 2) echo "no_edit"; ?> sub_area">
    <div style="min-height: 400px;">
      <div class="up_to_date" title="This section is up to date." <?php //echo (!$new_images && $imported != 1 ? "style='display:block;'" : "style='display:none;'") ?>><img src="images/up-to-date.png" width="35" height="35" style="vertical-align: middle;" alt="Up to Date"></div>
      <div class="attention" title="<?php //echo $attention_type_i; ?>" <?php //echo ($new_images == 1 && $imported != 1 ? "style='display:block;'" : "style='display:none;'") ?>><img src="images/attention.png" width="35" height="35" style="vertical-align: middle;" alt="Attention" class="attention_req"></div>
      <?php
//      if($imported == 1){
//        if($Listing_ID == "NA5377587207"){
//          echo "<br><br><img src='images/tutorial/gallery.jpg' width='221' height='167' alt='$address' class='listing_image_bg' />";
//        }else{
//          echo "<br><br><img src='".get_imported_image($ml_num, 0, 2)."' width='221' height='167' alt='$address' class='listing_image_bg' title='This listings photos were imported and cannot be changed on xTradeHomes.' />";
//        }
//      }else{
      ?>
      <div id="image_area" style="position:relative; min-height: 200px; padding-top: 25px; position:relative;">
        <div style='text-align: center; margin-top: 40px; position:absolute; z-index: 300; width: 900px; display:none;' id="preload_images">
            <img src='images/l_loader.gif' width='244' height='244' alt='Loading...'>
        </div>
        <h2 style="display:none;" id="image_area_title">Drag &amp; Drop images to Re-sort</h2>
          <div id="select_image" name="select_image" style="z-index: 100; height: 100px; width:965px; position:relative; float:left;"></div>
      </div>

  <!-- ********************************************************************************  -->
      <style>
      .image_crop > img { width: 100%; }
      </style>
      <div class="ddhandler">
      <!-- ********************************************************************************  -->
        <div class="instructions">
          <?php //if($browser['name'] == "Internet Explorer" && $browser['version'] <= 9){ ?>To Upload Photos
          <br>
          Use The Form To The Left
          <?php // }else{ ?>
          To Upload Photos Drag And Drop Images Here
          <br>
          Or Use The Form To The Left
          <?php // } ?>
          <br>
          Photos will be resized to 700px by 412px
        </div>
        <div id="crop_popup" style="opacity: 1; visibility: hidden; top: 10px;">
          <div id="sending_crop" name="sending_crop" style="display: none; position: absolute; top: 175px; left: 317px; z-index: 100;">
            <img src="images/l_loader.gif" width="250" height="250" alt="Loading...">
          </div>
          <div id="close_btn" class="cl_btn"><img src="images/close_image.png" width="18" height="17" alt="Close" /></div>
          <div class="top">
            <h2>Crop Image</h2>
          </div>
          <div id="image_content" style="width: 100%; max-width: 700px; margin-left: 15px; max-height: 412px;">
            <div class='image_crop' style="padding: 5px; margin-bottom: 5px; border: 1px solid #ccc;" id="c_image_holder">
              <img src="" name="" id="c_image" />
            </div>
            <div id='controls'>
              Hold &amp; drag image to reposition and crop and click 'Finished Editing' when done. Otherwise click 'Finished Editing' if OK.<br>
              <button id='zoom_out' type='button' class="buttons"> Zoom Out - </button>
              <button id='fit' type='button' class="buttons"> Fit Image [ ]  </button>
              <button id='zoom_in' type='button' class="buttons"> Zoom In + </button>
            </div>
            <input type="button" name="c_image_ok" id="c_image_ok" value="Finished Editing"  class="submit_button" />
          </div>
        </div>

        <div id="edit_image" style="display:none; min-height:400px; padding: 5px; margin-bottom: 50px; position: relative;">
          <div style='text-align: center; margin-top: 80px; position:absolute; z-index: 300; width: 900px; display:none;' id="preload_edits">
            <img src='images/l_loader.gif' width='244' height='244' alt='Loading...'>
          </div>
          <h2>Crop/Resize Uploaded Images</h2>
          <div id="editable_images"></div>
        </div><br><br>

        <div id="file_select" style="position:relative;margin-left: 18px;">
          <input name="images[]" id="image_files" type="file" accept="image/*" multiple class="image_handler"><br>
        </div>
        <br>
        <input type="button" name="file_submit" id="file_submit" value="Upload" style="float: left; margin-top: 50px; position: relative; z-index: 100;margin-left: 20px; cursor:pointer;"><br>
        <br>
        <div id="status1" style="float: left; margin-top: -18px; font-weight: bold; margin-left: -130px;"></div>
        <br>
        <div style="float: left; margin-top: -67px; margin-left: -213px;">
          Progress: <progress value="0" max="0" style="width: 200px; height: 20px;"></progress> <span id="curr"></span>
        </div>
        <br>
      </div>
      <?php //} ?>
    </div>

    <div class="submit_changes">
      <div class='section_complete multi_option' <?php echo ( !$new_has ? "style='display:block;'" : "style='display:none;'" ) ?>>
        <img src="images/up-to-date.png" width="35" height="35" style="vertical-align: middle;" alt="Up to Date">
      </div>
      <div class='check_all multi_option' <?php echo ( $new_has == 1 ? "style='display:block;'" : "style='display:none;'" ) ?>>
        <img src="images/attention.png" width="35" height="35" style="vertical-align: middle;" alt="Attention" class="attention_req">
      </div>
      <div class='successful multi_option' style="display:none;"></div>
    </div>

  </div>
</div>