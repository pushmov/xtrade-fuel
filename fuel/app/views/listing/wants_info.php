<div class="client_content" id="wants_info">
  <?php if($type_of_listing == 0){ ?><div id='match_pot'><?php=$matches;?></div><?php } ?>
  <div class="<?php if($type_of_listing == 1) echo "no_title_button"; else echo "title_button"; ?>">
    <div class="wants_sub_header title_box" <?php if($type_of_listing == 1) echo " title='This is a Sell Only listing. To change the listing for xTrading, go to member dashboard and change Type.'"; ?>>Client ‘WANTS’ <?php=($imported == 1 ? "<span style='font-size: 0.7em;'>ML#: ".$ml_num."</span>" : "");?>
      <a name="wants" id="wants"></a>
      <div class="attention_closed" title="<?php echo $attention_type_w; ?>" style="display:none;">
        <img src="images/attention.png" width="35" height="35" style="vertical-align: middle;" alt="Attention" class="attention_req">
      </div>
    </div>
  </div>
  <div class="client_content_sub <?php if($type_of_listing == 1) echo "no_edit"; ?> sub_area">
    <div class="up_to_date" title="This section is up to date." <?php echo ( !$new_wants ? "style='display:block;'" : "style='display:none;'" ) ?>>
      Information Up To Date <img src="images/up-to-date.png" width="35" height="35" style="vertical-align: middle;" alt="Up to Date">
    </div>
    <div class="attention" title="<?php echo $attention_type_w; ?>" <?php echo ( $new_wants == 1 ? "style='display:block;'" : "style='display:none;'" ) ?>>
      Please Complete All Required Fields <img src="images/attention.png" width="35" height="35" style="vertical-align: middle;" alt="Attention" class="attention_req">
    </div><br>
    <button id="wants_request" name="wants_request" class="buttons pop_button_ish" type="button" style="font-size: 1.1em; margin-left: 5px;">
      Request client complete their Wants
    </button><br>
    
    <div class="space" style="width:500px;">
      <label for='wants_address' class="req_text"><?=__('cpc_address'); ?><?=__('City/Province/State/Country');?><span style="color:#FF0000;">*</span></label>
    </div>
    <div class='wants_space'>
      <label for='neighbourhood_w' class="req_text" id='neighbourhood_w_label'><?=__('listing_wants_req') . " " . __('listing_neigh'); ?></label><span style="color:#FF0000;">*</span>
    </div><br>
    <div class="space" style="width:500px;">
      <input name="<?=$wants_address['field'];?>" type="text" class="input_field_ext req" id="wants_address" value="<?=$wants_address['value'];?>" placeholder="Start Typing City" size="24" />
    </div>
    <div class='wants_space'>
      <?=\Form::select($neighbourhood_w['field'], $neighbourhood_w['value'], $neighbourhood_w['options'], $neighbourhood_w['attr']);?>
    </div><br><br>
    <div class='space' style="width: 500px;">
      <label for='lprice_l_w' class="req_text" id="lprice_l_w_label"><?=__('listing_wants_req') . " " . __('listing_wants_price'); ?><span style="color:#FF0000;">*</span></label>
    </div>
    <div class='space'>
      <label for='sq_ft_w' class="req_text" id="sq_ft_w_label"><?=__('listing_wants_req') . " " . __('listing_sqfeet'); ?><span style="color:#FF0000;">*</span></label>
    </div><br />
    <div class='space' style="width: 500px;">
      <?php echo $search_between; ?>
        
      <?=\Form::select($lprice_l_w['field'], $lprice_l_w['value'], $lprice_l_w['options'], $lprice_l_w['attr']);?>
        
      <?php echo $search_and; ?> 
        
      <?=\Form::select($lprice_h_w['field'], $lprice_h_w['value'], $lprice_h_w['options'], $lprice_h_w['attr']);?>
        
    </div>
    <div class='space'>
        
      <?=\Form::select($sq_ft_w['field'], $sq_ft_w['value'], $sq_ft_w['options'], $sq_ft_w['attr']);?>
      
    </div><br><br>
    <div class='wants_space'>
      <label for='storeys_w[]' class="req_text" id="storeys_w_label"><?=__('listing_wants_req') . " " . __('listing_property'); ?><span style="color:#FF0000;">*</span></label>
    </div>
    <div class='wants_space'>
      <label for='bed_w' class="req_text" id="bed_w_label"><?=__('listing_wants_req') . " " . __('listing_beds'); ?><span style="color:#FF0000;">*</span></label>
    </div>
    <div class='wants_space'>
      <label for='bath_w' class="req_text" id="bath_w_label"><?=__('listing_wants_req') . " " . __('listing_baths'); ?><span style="color:#FF0000;">*</span></label>
    </div><br>
    <div class='wants_space'>
      <?=\Form::select($storeys_w['field'], $storeys_w['value'], $storeys_w['options'], $storeys_w['attr']);?>
    </div>
    
    
    <div class='wants_space'>
      <?=\Form::select($bed_w['field'], $bed_w['value'], $bed_w['options'], $bed_w['attr']);?>
    </div>
    
    <div class='wants_space'>
      <?=\Form::select($bath_w['field'], $bath_w['value'], $bath_w['options'], $bath_w['attr']);?>
    </div><br><br>
    <div class='wants_space'>
      <label for='p_title_w[]' class="req_text" id="p_title_w_label"><?=__('listing_wants_req') . " " . __('listing_title'); ?><span style="color:#FF0000;">*</span></label>
    </div>
    <div class='space'>
      <label for='prop_style_w[]' class="req_text" id="prop_style_w_label"><?=__('listing_wants_req') . " " . __('listing_storeys'); ?><span style="color:#FF0000;">*</span></label>
    </div><br>
    <div class='space'>
      <?=\Form::select($p_title_w['field'], $p_title_w['value'], $p_title_w['options'], $p_title_w['attr']);?>
    </div>
    <div class='space'>
      <?=\Form::select($prop_style_w['field'], $prop_style_w['value'], $prop_style_w['options'], $prop_style_w['attr']);?>
    </div><br><br>
    <div class='space'><?php echo $r_listing_wants_req . " " . $r_listing_lotsize; ?></div>
    <div class='space'><?php echo $r_listing_wants_req . " " . $r_listing_acreage; ?></div>
    <div class='space'><?php echo $r_listing_wants_req . " " . $r_listing_siding_type; ?></div>
    <br>
    
    <div class='space'>
      <?=\Form::select($t_lotsize_w['field'], $t_lotsize_w['value'], $t_lotsize_w['options'], $t_lotsize_w['attr']);?>
    </div>
    
    <div class='space'>
      <?=\Form::select($t_acreage_w['field'], $t_acreage_w['value'], $t_acreage_w['options'], $t_acreage_w['attr']);?>
    </div>
    <div class='space'>
      <?=\Form::select($siding_type_w['field'], $siding_type_w['value'], $siding_type_w['options'], $siding_type_w['attr']);?>
    </div>
    <br><br>
    
    <div class='space'>
      <label for='basement_w[]' class="" id="basement_w_label"><?=__('listing_wants_req') . " " . __('listing_basement_type'); ?></label>
    </div>
    <div class='wants_space'>
      <label for='garage_w[]' class="" id="garage_w_label"><?=__('listing_wants_req') . " " . __('listing_garage'); ?></label>
    </div>
    <div class='space'><?php echo $r_listing_wants_req . " " . $r_listing_view; ?></div>
    <br />
    
    <div class='space'>
      <?=\Form::select($basement_w['field'], $basement_w['value'], $basement_w['options'], $basement_w['attr']);?>
    </div>

    <div class='wants_space'>
      <?=\Form::select($garage_w['field'], $garage_w['value'], $garage_w['options'], $garage_w['attr']);?>
    </div>
    
    <div class='space'>
      <?=\Form::select($view_w['field'], $view_w['value'], $view_w['options'], $view_w['attr']);?>
    </div>
    <br><br>
    <div class='space'><?php echo $r_listing_wants_req . " " . $r_listing_heat_source ; ?></div>
    <div class='space'> <?php echo $r_listing_wants_req . " " . $r_listing_heat_type; ?></div>
    <div class='space'> <?php echo $r_listing_wants_req . " " . $r_listing_cool_type; ?></div>
    <br />
    
    
    <div class='space'>
      <?=\Form::select($heating_source_w['field'], $heating_source_w['value'], $heating_source_w['options'], $heating_source_w['attr']);?>
    </div>
    
    <div class='space'>
      <?=\Form::select($heating_w['field'], $heating_w['value'], $heating_w['options'], $heating_w['attr']);?>
    </div>
    <div class='space'>
      <?=\Form::select($cool_w['field'], $cool_w['value'], $cool_w['options'], $cool_w['attr']);?>
    </div>
    <br><br>
    <div class='space'> <?php echo $r_listing_wants_req . " " . $r_listing_water_type; ?></div>
    <div class='space'> <?php echo $r_listing_wants_req . " " . $r_listing_sewer_type; ?></div><br>
    <div class='space'>
      <?=\Form::select($water_w['field'], $water_w['value'], $water_w['options'], $water_w['attr']);?>
    </div>
    
    <div class='space'>
      <?=\Form::select($sewer_w['field'], $sewer_w['value'], $sewer_w['options'], $sewer_w['attr']);?>
    </div>
    <br><br>
    <div class='space'><?php echo $r_listing_wants_req . " " . $r_listing_floor_type; ?></div>
    <div class='space'> <?php echo $r_listing_wants_req . " " . $r_listing_roof_type; ?></div><br>
    
    <div class='space'>
      <?=\Form::select($floor_type_w['field'], $floor_type_w['value'], $floor_type_w['options'], $floor_type_w['attr']);?>
    </div>
    <div class='space'>
      <?=\Form::select($roof_type_w['field'], $roof_type_w['value'], $roof_type_w['options'], $roof_type_w['attr']);?>
    </div>
    <br><br>
        
    <div style="height: 20px;" id="check_area" name="check_area">
      <div id='condo_set_w' style="display:none;">
        <div class="check_sections"></div>
        <div class="check_sections" style="margin-right: 633px; height: 0px;"></div>
      </div>
      <div class="check_sections"></div>
      <div class="check_sections"></div>
      <div class="check_sections"></div>
      <div class="check_sections"></div>
      <div class="check_sections"></div>
    </div>
    <!--End imported information-->
    <br><br><br>
    <div class="submit_changes">
      <div class='section_complete multi_option' <?php echo ( !$new_wants ? "style='display:block;'" : "style='display:none;'" ) ?>>
        Information Up To Date <img src="images/up-to-date.png" width="35" height="35" style="vertical-align: middle;" alt="Up to Date">
      </div>
      <div class='check_all multi_option' <?php echo ( $new_wants == 1 ? "style='display:block;'" : "style='display:none;'" ) ?>>
        Please Complete All Required Fields <img src="images/attention.png" width="35" height="35" style="vertical-align: middle;" alt="Attention" class="attention_req">
      </div>
      <div class='successful multi_option' style="display:none;">Update Successful</div>
    </div> 
  </div>
</div>