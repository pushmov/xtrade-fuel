<div class="client_content" id="has_info">
  <div class="<?php if($type_of_listing == 2) echo "no_title_button"; else echo "title_button"; ?>">
    <div class="wants_sub_header title_box"<?php if($type_of_listing == 2){ echo "title='This is a buyers only listing. This area is locked.'"; }elseif($imported && $type_of_listing == 1){ echo "title='You can make changes to the details on this property but they will not appear in any public display, and only used for matching xTrading purposes.'"; }?>>Client ‘HAS’ <?php($imported == 1 ? "<span style='font-size: 0.7em;'>ML#: ".$ml_num."</span>" : "");?>
      <a name="has" id="has"></a>
      <div class="attention_closed" title="<?php echo $attention_type_h; ?>" style="display:none;">
          <img src="images/attention.png" width="35" height="35" style="vertical-align: middle;" alt="Attention" class="attention_req">
      </div>
    </div>
  </div>
  <div class="client_content_sub <?php if($type_of_listing == 2) echo "no_edit"; ?> sub_area">
    <div class="up_to_date" title="This section is up to date." <?php echo ( !$new_has ? "style='display:block;'" : "style='display:none;'" ) ?>>Information Up To Date 
      <img src="images/up-to-date.png" width="35" height="35" style="vertical-align: middle;" alt="Up to Date">
    </div>
    <div class="attention" title="<?php echo $attention_type_h; ?>" <?php echo ( $new_has == 1 ? "style='display:block;'" : "style='display:none;'" ) ?>>Please Complete All Required Fields 
       <img src="images/attention.png" width="35" height="35" style="vertical-align: middle;" alt="Attention" class="attention_req">
    </div><br><br>
    <div class='wants_space' style="width: 463px;">
        <label for='has_paddress' class="req_text"><?=__('Property Address');?><span style="color:#FF0000;">*</span></label>
    </div>
    <div class="space" style="">
        <label for='has_address' class="req_text"><?=__('City/Province/State/Country');?><span style="color:#FF0000;">*</span></label>
    </div><br />
    <div class='wants_space' style="width: 463px;">
      <input name="<?=$has_paddress['field'];?>" type="text" class="input_field_ext req" id="<?=$has_paddress['field'];?>" value="<?=$has_paddress['value'];?>" <?=$has_paddress['disabled'];?> />
    </div>
    <div class="space">
      <input name="<?=$has_address['field'];?>" type="text" class="input_field_ext req" id="<?=$has_address['field'];?>" value="<?=$has_address['value'];?>" placeholder="Start Typing City" size="24" <?=$has_address['disabled'];?> />
    </div><br><br>
    <div class='wants_space' style="width: 463px;">
      <label for='neighbourhood_h' class="req_text" id="neighbourhood_h_label"><?=__('listing_wants_req') . " " . __('listing_neigh');?>
          <span style="color:#FF0000;">*</span>
      </label>
    </div>
    <div class='wants_space'>
      <label for='z_pcode_h' class="req_text"><?=__('listing_zip');?><span style="color:#FF0000;">*</span></label>
    </div><br>
    <div class='wants_space' style="width: 463px;">
      <?=\Form::select($neighbourhood_h['field'], $neighbourhood_h['value'], $neighbourhood_h['options'], $neighbourhood_h['attr']);?>
    </div>
    <div class='wants_space'>
      <input name="<?=$z_pcode_h['field'];?>" type="text" class="<?php echo $creq6; ?> req" id="z_pcode_h" value="<?=$z_pcode_h['value'];?>" <?=$z_pcode_h['disabled'];?> />
    </div><br><br>
    <div class='space' style="width: 463px;">
        <label for='lprice_h' class="req_text"><?=__('listing_price');?><span style="color:#FF0000;">*</span></label>
    </div>
    <div class='space'>
      <label for='sq_ft_h' class="req_text"><?=__('listing_sqfeet');?><span style="color:#FF0000;">*</span></label>
    </div><br>
    <div class='space' style="width: 463px;">
      $<input type="text" name="<?=$lprice_h['field'];?>" id="lprice_h" value="<?=$lprice_h['value'];?>" size='15' class='<?php echo $req24; ?> req' style="width: 75px;" />
    </div>
    <div class='space'>
        <input type="text" name="<?=$sq_ft_h['field'];?>" id="sq_ft_h" value="<?=$sq_ft_h['value'] ?>" size='15' class='<?php echo $req23; ?> req' style="width: 75px;" /><?php echo $r_listing_sqf; ?>
    </div><br><br>
    <div class='space'>
        <label for='storeys_h' class="req_text"><?=__('listing_property');?><span style="color:#FF0000;">*</span></label>
    </div>
    <div class='space'>
        <label for='bed_h' class="req_text"><?=__('listing_bed');?><span style="color:#FF0000;">*</span></label>
    </div>
    <div class='space'>
        <label for='bath_h' class="req_text"><?=__('listing_bath'); ?><span style="color:#FF0000;">*</span></label>
    </div><br />
    <div class='space'>
      <?=\Form::select($storeys_h['field'], $storeys_h['value'], $storeys_h['options'], $storeys_h['attr']);?>
    </div>
    <div class='space'>
        <input type="text" name="<?=$bed_h['field'];?>" id="bed_h" value="<?=$bed_h['value'] ?>" class='req' size="2" maxlength="2" style="width: 25px;" />
    </div>
    <div class='space'>
        <input type="text" name="<?=$bath_h['field'];?>" id="bath_h" value="<?=$bath_h['value'] ?>" class='req' size="3" maxlength="3" style="width: 25px;" />
    </div><br /><br />
    <div class='space'>
        <label for='p_title_h' class="req_text" id="p_title_h_label"><?=__('listing_title'); ?><span style="color:#FF0000;">*</span></label>
    </div>
    <div class='space'>
        <label for='prop_style_h' class="req_text"><?=__('listing_storeys'); ?><span style="color:#FF0000;">*</span></label>
    </div><br />
    <div class='space'>
      <?=\Form::select($p_title_h['field'], $p_title_h['value'], $p_title_h['options'], $p_title_h['attr']);?>
    </div>
    
    <div class='space'>
      <?=\Form::select($prop_style_h['field'], $prop_style_h['value'], $prop_style_h['options'], $prop_style_h['attr']);?>
    </div>
    <br /><br />
    
    <div class='space'><?php echo $r_listing_lotsize; ?></div>
    <div class='space'><?php echo $r_listing_acreage; ?></div>
    <div class='space'><?php echo $r_listing_siding_type ; ?></div><br />
    <div class='space'>
        <input type="text" name="<?=$lot_size_h['field'];?>" id="lot_size_h" value="<?php $lot_size_h['value'];?>" size='15' class='input_boxs' style="width: 75px;" /><?php echo $r_listing_sqf; ?>
    </div>
    <div class='space'>
        <input type="text" name="<?=$t_acreage_h['field'];?>" id="t_acreage_h" value="<?php $t_acreage_h['value'];?>" size='15' class='input_boxs' style="width: 75px;" /><?php echo $r_listing_has_acres; ?>
    </div>
    <div class='space'>
      <?=\Form::select($p_siding_type_h['field'], $p_siding_type_h['value'], $p_siding_type_h['options'], $p_siding_type_h['attr']);?>
      
    </div><br /><br />
    <div class='space'>
        <label for='basement_h' class="req_text" id="basement_h_label"><?=__('listing_basement'); ?><span style="color:#FF0000;">*</span></label>
    </div>
    <div class='space'>
        <label for='garage_h' class="req_text" id="garage_h_label"><?=__('listing_garage'); ?><span style="color:#FF0000;">*</span></label>
    </div>
    <div class='space'>
        <label for='view_h'><?=__('listing_view'); ?></label>
    </div><br />
    <div class='space'>
      <?=\Form::select($p_basement_h['field'], $p_basement_h['value'], $p_basement_h['options'], $p_basement_h['attr']);?>
    </div>
    <div class='space'>
      <?=\Form::select($p_garage_h['field'], $p_garage_h['value'], $p_garage_h['options'], $p_garage_h['attr']);?>
    </div>
    <div class='space'>
      <?=\Form::select($p_view_h['field'], $p_view_h['value'], $p_view_h['options'], $p_view_h['attr']);?>
    </div>
    <br /><br />
    
    <div class='space'><?php echo $r_listing_heat_source ; ?></div>
    <div class='space'><?php echo $r_listing_heat_type ; ?></div>
    <div class='space'><?php echo $r_listing_cool_type ; ?></div><br />
    
    <div class='space'>
      <?=\Form::select($p_heating_source_h['field'], $p_heating_source_h['value'], $p_heating_source_h['options'], $p_heating_source_h['attr']);?>
    </div>
    <div class='space'>
      <?=\Form::select($p_heating_h['field'], $p_heating_h['value'], $p_heating_h['options'], $p_heating_h['attr']);?>
    </div>
    <div class='space'>
      <?=\Form::select($p_cool_h['field'], $p_cool_h['value'], $p_cool_h['options'], $p_cool_h['attr']);?>    
    </div>
    <br /><br />
    
    <div class='space'><?php echo $r_listing_water_type; ?></div>
    <div class='space'><?php echo $r_listing_sewer_type; ?></div><br />
    <div class='space'>
      <?=\Form::select($p_water_h['field'], $p_water_h['value'], $p_water_h['options'], $p_water_h['attr']);?>
    </div>
    <div class='space'>
      <?=\Form::select($p_sewer_h['field'], $p_sewer_h['value'], $p_sewer_h['options'], $p_sewer_h['attr']);?>
    </div><br /><br />
    <div class='space'><?php echo $r_listing_floor_type; ?></div>
    <div class='space'><?php echo $r_listing_roof_type; ?></div><br />
    <div class='space'>
      <?=\Form::select($p_floor_type_h['field'], $p_floor_type_h['value'], $p_floor_type_h['options'], $p_floor_type_h['attr']);?>
    </div>
    <div class='space'>
      <?=\Form::select($p_roof_type_h['field'], $p_roof_type_h['value'], $p_roof_type_h['options'], $p_roof_type_h['attr']);?>
    </div>
    
    <br /><br />
    
    <div class='space'><label for='yr_built_h'><?=__('listing_ybuilt'); ?></label></div>
    <div class='space'><?php echo $r_listing_ptax; ?></div>
    <div class='space' id='strata_fees_name_h'  style="display:none;"><?php echo $r_listing_has_strata; ?></div>
    <div class='space'><?php echo $r_listing_lsd; ?></div><br />
    <div class='space'>
      <?=\Form::select($yr_built_h['field'], $yr_built_h['value'], $yr_built_h['options'], $yr_built_h['attr']);?>
    </div>
    
    <div class='space'>
        $<input type="text" name="<?=$prop_tax_h['field'];?>" id="prop_tax_h" value="<?=$prop_tax_h['value'];?>" size='15' class='<?php// echo $req_prop_tax_h; ?>' style="width: 75px;" />
    </div>
    <div class='space' id='strata_fees_h'  style="display:none;">
        $<input type="text" name="<?=$strata_fees_h['field'];?>" id="strata_fees_h" value="<?=$strata_fees_h['value'];?>" size='15' class='input_boxs'  style="width: 75px;" />
    </div>
    <div class='space ui-widget'>
        <input type="text" name="school_h" id="school_h" value="<?=$school_h['value'];?>" size='15' class='input_boxs' />
    </div><br /><br />
    
    <?php if($if_exists[0] == 2){ ?>
    <div class='space'><?php echo $r_listing_has_yt; ?></div>
    <div class='space'><?php echo $r_listing_has_vtl; ?></div>
    <br />
    <div class='space'><input type="text" name="<?=$yt['field'];?>" id="yt" value="<?=$yt['value'] ?>" size='25' class='input_boxs' <?=$yt['disabled'];?>></div>
    <div class='space'><input type="text" name="<?=$vtl['field'];?>" id="vtl" value="<?=$vtl['value'];?>" size='25' class='input_boxs' <?=$vtl['disabled'];?>></div>
    <br /><br /><br />
    <?php } ?>
    <div id='condo_set_h' style="display:none;">
      <div class="check_sections">
        <strong><?php echo $r_listing_has_strata_inc; ?>:</strong><br />
          <div class='check_space'>
              <?=\Form::checkbox($c_confeat['cable_h']['field'], $c_confeat['value'], $c_confeat['cable_h']['checked'], $c_confeat['cable_h']['options']);?>?>
              <?=$c_confeat['cable_h']['label'];?>
          </div>
          <div class='check_space'>
              <?=\Form::checkbox($c_confeat['caretaker_h']['field'], $c_confeat['value'], $c_confeat['caretaker_h']['checked'], $c_confeat['caretaker_h']['options']);?> 
              <?=$c_confeat['caretaker_h']['label'];?>
          </div>
          <div class='check_space'>
              <?=\Form::checkbox($c_confeat['garbage_pickup_h']['field'], $c_confeat['value'], $c_confeat['garbage_pickup_h']['checked'], $c_confeat['garbage_pickup_h']['options']);?> 
              <?=$c_confeat['garbage_pickup_h']['label'];?>
          </div>
          <div class='check_space'>
              <?=\Form::checkbox($c_confeat['heat_h']['field'], $c_confeat['value'], $c_confeat['heat_h']['checked'], $c_confeat['heat_h']['options']);?> 
              <?=$c_confeat['heat_h']['label'];?>
          </div>
          <div class='check_space'>
              <?=\Form::checkbox($c_confeat['hot_water_h']['field'], $c_confeat['value'], $c_confeat['hot_water_h']['checked'], $c_confeat['hot_water_h']['options']);?> 
              <?=$c_confeat['hot_water_h']['label'];?>
          </div>
          <div class='check_space'>
              <?=\Form::checkbox($c_confeat['management_h']['field'], $c_confeat['value'], $c_confeat['management_h']['checked'], $c_confeat['management_h']['options']);?> 
              <?=$c_confeat['management_h']['label'];?>
          </div>
          <div class='check_space'>
              <?=\Form::checkbox($c_confeat['snow_removal_h']['field'], $c_confeat['value'], $c_confeat['snow_removal_h']['checked'], $c_confeat['snow_removal_h']['options']);?> 
              <?=$c_confeat['snow_removal_h']['label'];?>
          </div>
          <br /><br />
          <div class='check_space'>
              <?=\Form::checkbox($c_confeat['water_inc_h']['field'], $c_confeat['value'], $c_confeat['water_inc_h']['checked'], $c_confeat['water_inc_h']['options']);?> 
              <?=$c_confeat['water_inc_h']['label'];?>
          </div>
          <div class='check_space'>
              <?=\Form::checkbox($c_confeat['electric_h']['field'], $c_confeat['value'], $c_confeat['electric_h']['checked'], $c_confeat['electric_h']['options']);?> 
              <?=$c_confeat['electric_h']['label'];?>
          </div>
          <div class='check_space'>
              <?=\Form::checkbox($c_confeat['other6_h']['field'], $c_confeat['value'], $c_confeat['other6_h']['checked'], $c_confeat['other6_h']['options']);?> 
              <?=$c_confeat['other6_h']['label'];?>
          </div>
          <br /><br />
      </div>
      <div class="check_sections" style="margin-right: 633px; height: 405px;">
        <strong><?php echo $r_listing_has_strata_rest; ?></strong><br />
          <div class='check_space'>
              <?=\Form::checkbox($strata_strict_h['age_strict_h']['field'], $strata_strict_h['value'], $strata_strict_h['age_strict_h']['checked'], $strata_strict_h['age_strict_h']['options']);?>
              <?=$strata_strict_h['age_strict_h']['label'];?>
          </div>
          <div class='check_space'>
              <?=\Form::checkbox($strata_strict_h['no_pets_allowed_h']['field'], $strata_strict_h['value'], $strata_strict_h['no_pets_allowed_h']['checked'], $strata_strict_h['no_pets_allowed_h']['options']);?>
              <?=$strata_strict_h['no_pets_allowed_h']['label'];?>
          </div>
          <div class='check_space'>
              <?=\Form::checkbox($strata_strict_h['any_pets_allowed_h']['field'], $strata_strict_h['value'], $strata_strict_h['any_pets_allowed_h']['checked'], $strata_strict_h['any_pets_allowed_h']['options']);?>
              <?=$strata_strict_h['any_pets_allowed_h']['label'];?>
          </div>
          <div class='check_space'>
              <?=\Form::checkbox($strata_strict_h['pets_allowed_strict_h']['field'], $strata_strict_h['value'], $strata_strict_h['pets_allowed_strict_h']['checked'], $strata_strict_h['pets_allowed_strict_h']['options']);?>
              <?=$strata_strict_h['pets_allowed_strict_h']['label'];?>
          </div><br /><br />
          <div class='check_space'>
              <?=\Form::checkbox($strata_strict_h['is_not_rentable_h']['field'], $strata_strict_h['value'], $strata_strict_h['is_not_rentable_h']['checked'], $strata_strict_h['is_not_rentable_h']['options']);?>
              <?=$strata_strict_h['is_not_rentable_h']['label'];?>
          </div>
          <div class='check_space'>
              <?=\Form::checkbox($strata_strict_h['is_rentable_h']['field'], $strata_strict_h['value'], $strata_strict_h['is_rentable_h']['checked'], $strata_strict_h['is_rentable_h']['options']);?>
              <?=$strata_strict_h['is_rentable_h']['label'];?>
          </div>
          <div class='check_space'>
              <?=\Form::checkbox($strata_strict_h['is_rentable_strict_h']['field'], $strata_strict_h['value'], $strata_strict_h['is_rentable_strict_h']['checked'], $strata_strict_h['is_rentable_strict_h']['options']);?>
              <?=$strata_strict_h['is_rentable_strict_h']['label'];?>
          </div>
          <div class='check_space'>
              <?=\Form::checkbox($strata_strict_h['no_strict_h']['field'], $strata_strict_h['value'], $strata_strict_h['no_strict_h']['checked'], $strata_strict_h['no_strict_h']['options']);?>
              <?=$strata_strict_h['no_strict_h']['label'];?>
          </div>
          <br /><br />
      </div>
    </div>
    <div class="check_sections">
      <strong><?php echo $r_listing_ifeature; ?></strong><br />
      <div class='check_space'>
        <?=\Form::checkbox($int_feat_h['fireplace_h']['field'], $int_feat_h['value'], $int_feat_h['fireplace_h']['checked'], $int_feat_h['fireplace_h']['options']);?>
        <?=$int_feat_h['fireplace_h']['label'];?>
      </div>
      <div class='check_space'>
        <?=\Form::checkbox($int_feat_h['ceiling_fans_h']['field'], $int_feat_h['value'], $int_feat_h['ceiling_fans_h']['checked'], $int_feat_h['ceiling_fans_h']['options']);?>
        <?=$int_feat_h['ceiling_fans_h']['label'];?>
      </div>
      <div class='check_space'>
        <?=\Form::checkbox($int_feat_h['attic_h']['field'], $int_feat_h['value'], $int_feat_h['attic_h']['checked'], $int_feat_h['attic_h']['options']);?>
        <?=$int_feat_h['attic_h']['label'];?>
      </div>
      <div class='check_space'>
        <?=\Form::checkbox($int_feat_h['intercom_h']['field'], $int_feat_h['value'], $int_feat_h['intercom_h']['checked'], $int_feat_h['intercom_h']['options']);?>
        <?=$int_feat_h['intercom_h']['label'];?>
      </div>
      <div class='check_space'>
        <?=\Form::checkbox($int_feat_h['jetted_tub_h']['field'], $int_feat_h['value'], $int_feat_h['jetted_tub_h']['checked'], $int_feat_h['jetted_tub_h']['options']);?>
        <?=$int_feat_h['jetted_tub_h']['label'];?>
      </div><br /><br />
      <div class='check_space'>
        <?=\Form::checkbox($int_feat_h['security_system_h']['field'], $int_feat_h['value'], $int_feat_h['security_system_h']['checked'], $int_feat_h['security_system_h']['options']);?>
        <?=$int_feat_h['security_system_h']['label'];?>
      </div>
      <div class='check_space'>
        <?=\Form::checkbox($int_feat_h['skylights_h']['field'], $int_feat_h['value'], $int_feat_h['skylights_h']['checked'], $int_feat_h['skylights_h']['options']);?>
        <?=$int_feat_h['skylights_h']['label'];?>
      </div>
      <div class='check_space'>
        <?=\Form::checkbox($int_feat_h['vaulted_ceiling_h']['field'], $int_feat_h['value'], $int_feat_h['vaulted_ceiling_h']['checked'], $int_feat_h['vaulted_ceiling_h']['options']);?>
        <?=$int_feat_h['vaulted_ceiling_h']['label'];?>
      </div>
      <div class='check_space'>
        <?=\Form::checkbox($int_feat_h['other1_h']['field'], $int_feat_h['value'], $int_feat_h['other1_h']['checked'], $int_feat_h['other1_h']['options']);?>
        <?=$int_feat_h['other1_h']['label'];?>
      </div><br /><br />
    </div>
    <div class="check_sections">
      <strong><?php echo $r_listing_kfeature; ?></strong><br />
      <div class='check_space'>
        <?=\Form::checkbox($kitchen_h['fridge_h']['field'], $kitchen_h['value'], $kitchen_h['fridge_h']['checked'], $kitchen_h['fridge_h']['options']);?>
        <?=$kitchen_h['fridge_h']['label'];?>
      </div>
      <div class='check_space'>
        <?=\Form::checkbox($kitchen_h['stove_h']['field'], $kitchen_h['value'], $kitchen_h['stove_h']['checked'], $kitchen_h['stove_h']['options']);?>
        <?=$kitchen_h['stove_h']['label'];?>
      </div>
      <div class='check_space'>
        <?=\Form::checkbox($kitchen_h['built_in_oven_h']['field'], $kitchen_h['value'], $kitchen_h['built_in_oven_h']['checked'], $kitchen_h['built_in_oven_h']['options']);?>
        <?=$kitchen_h['built_in_oven_h']['label'];?>
      </div>
      <div class='check_space'>
        <?=\Form::checkbox($kitchen_h['microwave_h']['field'], $kitchen_h['value'], $kitchen_h['microwave_h']['checked'], $kitchen_h['microwave_h']['options']);?>
        <?=$kitchen_h['microwave_h']['label'];?>
      </div><br /><br />
      <div class='check_space'>
        <?=\Form::checkbox($kitchen_h['Dishwasher_h']['field'], $kitchen_h['value'], $kitchen_h['Dishwasher_h']['checked'], $kitchen_h['Dishwasher_h']['options']);?>
        <?=$kitchen_h['Dishwasher_h']['label'];?>
      </div>
      <div class='check_space'>
        <?=\Form::checkbox($kitchen_h['Garburator_h']['field'], $kitchen_h['value'], $kitchen_h['Garburator_h']['checked'], $kitchen_h['Garburator_h']['options']);?>
        <?=$kitchen_h['Garburator_h']['label'];?>
      </div>
      <div class='check_space'>
        <?=\Form::checkbox($kitchen_h['Trash_Compactor_h']['field'], $kitchen_h['value'], $kitchen_h['Trash_Compactor_h']['checked'], $kitchen_h['Trash_Compactor_h']['options']);?>
        <?=$kitchen_h['Trash_Compactor_h']['label'];?>
      </div>
      <div class='check_space'>
        <?=\Form::checkbox($kitchen_h['other2_h']['field'], $kitchen_h['value'], $kitchen_h['other2_h']['checked'], $kitchen_h['other2_h']['options']);?>
        <?=$kitchen_h['other2_h']['label'];?>
      </div>
      <br /><br />
    </div>
    <div class="check_sections">
      <strong><?php echo $r_listing_lfeatures; ?></strong><br />
      <div class='check_space'>
        <?=\Form::checkbox($laundry_h['Washer_h']['field'], $laundry_h['value'], $laundry_h['Washer_h']['checked'], $laundry_h['Washer_h']['options']);?>
        <?=$laundry_h['Washer_h']['label'];?>
      </div>
      <div class='check_space'>
        <?=\Form::checkbox($laundry_h['Dryer_h']['field'], $laundry_h['value'], $laundry_h['Dryer_h']['checked'], $laundry_h['Dryer_h']['options']);?>
        <?=$laundry_h['Dryer_h']['label'];?>
      </div>
      <div class='check_space'>
        <?=\Form::checkbox($laundry_h['Washer_Dryer_Combo_h']['field'], $laundry_h['value'], $laundry_h['Washer_Dryer_Combo_h']['checked'], $laundry_h['Washer_Dryer_Combo_h']['options']);?>
        <?=$laundry_h['Washer_Dryer_Combo_h']['label'];?>
      </div>
      <div class='check_space'>
        <?=\Form::checkbox($laundry_h['Laundry_Sink_h']['field'], $laundry_h['value'], $laundry_h['Laundry_Sink_h']['checked'], $laundry_h['Laundry_Sink_h']['options']);?>
        <?=$laundry_h['Laundry_Sink_h']['label']; 
        ?>
      </div>
      <div class='check_space'>
        <?=\Form::checkbox($laundry_h['other3_h']['field'], $laundry_h['value'], $laundry_h['other3_h']['checked'], $laundry_h['other3_h']['options']);?>
        <?=$laundry_h['other3_h']['label'];?>
      </div>
      <br /><br />
    </div>
    <div class="check_sections">
      <strong><?php echo $r_listing_pfeature; ?></strong><br />
      <div class='check_space'>
        <?=\Form::checkbox($prop_feat_h['pool_h']['field'], $prop_feat_h['value'], $prop_feat_h['pool_h']['checked'], $prop_feat_h['pool_h']['options']);?>
        <?=$prop_feat_h['pool_h']['label'];?>
      </div>
      <div class='check_space'>
        <?=\Form::checkbox($prop_feat_h['acreage_h']['field'], $prop_feat_h['value'], $prop_feat_h['acreage_h']['checked'], $prop_feat_h['acreage_h']['options']);?>
        <?=$prop_feat_h['acreage_h']['label'];?>
      </div>
      <div class='check_space'>
        <?=\Form::checkbox($prop_feat_h['work_shop_h']['field'], $prop_feat_h['value'], $prop_feat_h['work_shop_h']['checked'], $prop_feat_h['work_shop_h']['options']);?>
        <?=$prop_feat_h['work_shop_h']['label'];?>
      </div>
      <div class='check_space'>
        <?=\Form::checkbox($prop_feat_h['deck_h']['field'], $prop_feat_h['value'], $prop_feat_h['deck_h']['checked'], $prop_feat_h['deck_h']['options']);?>
        <?=$prop_feat_h['deck_h']['label'];?>
      </div><br /><br />
      <div class='check_space'>
        <?=\Form::checkbox($prop_feat_h['dock_h']['field'], $prop_feat_h['value'], $prop_feat_h['dock_h']['checked'], $prop_feat_h['dock_h']['options']);?>
        <?=$prop_feat_h['dock_h']['label'];?>
      </div>
      <div class='check_space'>
        <?=\Form::checkbox($prop_feat_h['greenhouse_h']['field'], $prop_feat_h['value'], $prop_feat_h['greenhouse_h']['checked'], $prop_feat_h['greenhouse_h']['options']);?>
        <?=$prop_feat_h['greenhouse_h']['label'];?>
      </div>
      <div class='check_space'>
        <?=\Form::checkbox($prop_feat_h['hot_tub_spa_h']['field'], $prop_feat_h['value'], $prop_feat_h['hot_tub_spa_h']['checked'], $prop_feat_h['hot_tub_spa_h']['options']);?>
        <?=$prop_feat_h['hot_tub_spa_h']['label'];?>
      </div>
      <div class='check_space'>
        <?=\Form::checkbox($prop_feat_h['sauna_h']['field'], $prop_feat_h['value'], $prop_feat_h['sauna_h']['checked'], $prop_feat_h['sauna_h']['options']);?>
        <?=$prop_feat_h['sauna_h']['label'];?>
      </div><br /><br />
      <div class='check_space'>
        <?=\Form::checkbox($prop_feat_h['pond_h']['field'], $prop_feat_h['value'], $prop_feat_h['pond_h']['checked'], $prop_feat_h['pond_h']['options']);?>
        <?=$prop_feat_h['pond_h']['label'];?>
      </div>
      <div class='check_space'>
        <?=\Form::checkbox($prop_feat_h['sprinkler_system_h']['field'], $prop_feat_h['value'], $prop_feat_h['sprinkler_system_h']['checked'], $prop_feat_h['sprinkler_system_h']['options']);?>
        <?=$prop_feat_h['sprinkler_system_h']['label'];?>
      </div>
      <div class='check_space'>
        <?=\Form::checkbox($prop_feat_h['water_front_h']['field'], $prop_feat_h['value'], $prop_feat_h['water_front_h']['checked'], $prop_feat_h['water_front_h']['options']);?>
        <?=$prop_feat_h['water_front_h']['label'];?>
      </div>
      <div class='check_space'>
        <?=\Form::checkbox($prop_feat_h['patio_h']['field'], $prop_feat_h['value'], $prop_feat_h['patio_h']['checked'], $prop_feat_h['patio_h']['options']);?>
        <?=$prop_feat_h['patio_h']['label'];?>
      </div><br /><br />
      <div class='check_space'>
        <?=\Form::checkbox($prop_feat_h['other4_h']['field'], $prop_feat_h['value'], $prop_feat_h['other4_h']['checked'], $prop_feat_h['other4_h']['options']);?>
        <?=$prop_feat_h['other4_h']['label'];?>
      </div>
      <br /><br />
    </div>
    <div class="check_sections">
      <strong><?php echo $r_listing_nfeature; ?></strong><br />
      <div class='check_space'>
        <?=\Form::checkbox($neigh_feat_h['community_pool_h']['field'], $neigh_feat_h['value'], $neigh_feat_h['community_pool_h']['checked'], $neigh_feat_h['community_pool_h']['options']);?>
        <?=$neigh_feat_h['community_pool_h']['label'];?>
      </div>
      <div class='check_space'>
        <?=\Form::checkbox($neigh_feat_h['gated_community_h']['field'], $neigh_feat_h['value'], $neigh_feat_h['gated_community_h']['checked'], $neigh_feat_h['gated_community_h']['options']);?>
        <?=$neigh_feat_h['gated_community_h']['label'];?>
      </div>
      <div class='check_space'>
        <?=\Form::checkbox($neigh_feat_h['golf_course_location_h']['field'], $neigh_feat_h['value'], $neigh_feat_h['golf_course_location_h']['checked'], $neigh_feat_h['golf_course_location_h']['options']);?>
        <?=$neigh_feat_h['golf_course_location_h']['label'];?>
      </div><br /><br />
      <div class='check_space'>
        <?=\Form::checkbox($neigh_feat_h['clubhouse_h']['field'], $neigh_feat_h['value'], $neigh_feat_h['clubhouse_h']['checked'], $neigh_feat_h['clubhouse_h']['options']);?>
          echo $neigh_feat_h['clubhouse_h']['label']; 
        ?>
      </div>
      <div class='check_space'>
        <?=\Form::checkbox($neigh_feat_h['water_front1_h']['field'], $neigh_feat_h['value'], $neigh_feat_h['water_front1_h']['checked'], $neigh_feat_h['water_front1_h']['options']);?>
        <?=$neigh_feat_h['water_front1_h']['label'];?>
      </div>
      <div class='check_space'>
        <?=\Form::checkbox($neigh_feat_h['other5_h']['field'], $neigh_feat_h['value'], $neigh_feat_h['other5_h']['checked'], $neigh_feat_h['other5_h']['options']);?>
        <?=$neigh_feat_h['other5_h']['label'];?>
      </div>
      <br /><br />
    </div><br />
    <div class="has_comments">
      <?php echo $r_listing_has_add_comments; ?><br />
      <textarea name="comments_h" id="comments_h" cols="114" rows="12" class='comment_box' style="width: 900px; margin-left: 30px;" <?= $imported ? "disabled" : ""; ?>><?php echo $comments_h ?></textarea>
      <br />
    </div>
    <!-- /Imported Information From Old Form-->
    <br>
    <div class="submit_changes"><div class='section_complete multi_option' <?php// echo ( !$new_has ? "style='display:block;'" : "style='display:none;'" ) ?>>Information Up To Date <img src="images/up-to-date.png" width="35" height="35" style="vertical-align: middle;" alt="Up to Date"></div><div class='check_all multi_option' <?php //echo ( $new_has == 1 ? "style='display:block;'" : "style='display:none;'" ) ?>>Please Complete All Required Fields <img src="images/attention.png" width="35" height="35" style="vertical-align: middle;" alt="Attention" class="attention_req"></div><div class='successful multi_option' style="display:none;">Update Successful</div></div>
  </div>
</div>