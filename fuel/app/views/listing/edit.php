<div class="common_content">
  <? //include("inc/member_subheader.php"); ?>
    <div class="green_line"></div>
    <form id="main_form" name="main_form" method="post" action="/listing/update/<?php echo $listing_id; ?>" >
      <input name="Listing_ID" type="hidden" value="<?=$listing_id; ?>">
      <input name="sub_type" id="sub_type" type="hidden" value="<?=$sub_type; ?>">
        <?=($tut ? "<div class='tutorial_overlay'></div>" : "");?>
        <div class="submit_overlay"></div>
        <div style="position: fixed; bottom: 70px; margin-left: 755px; z-index: 100;">
        	<button class='wants_buttons submit_content' style="display:none;" id="submit_content" name="submit_content" type="button">Save Now</button>
        </div>
        <div style="position: fixed; bottom: 70px; margin-left: 755px; z-index: 100; display:none;" id="_message">
        	<button class='wants_buttons submit_content' id="scroll_to_next" style="height: 45px;" name="scroll_to_next" type="button">Almost Finished! Go To Next Area</button>
        </div>
        <div style="position: fixed; bottom: 25px; margin-left: 755px; z-index: 100;">
        	<button class='wants_buttons return_home' style="display:block;" id="return_home" name="return_home" type="button">Return to Home</button>
        </div>
        <div class='submitting multi_option' style="position: fixed; bottom: 70px; margin-left: 718px; z-index: 100; display:none;">
        	<img src='images/preload.gif' width='36' height='35' style='vertical-align: middle;'> Please Wait, Submitting Content
        </div>    
        
        <?php echo $client_info; ?>
        <?php echo $has_info; ?>
        <?php echo $image_info; ?>
        <?php echo $wants_info; ?>
        
    </form>
    
    <span id="listing_id" data-value="<?php echo $listing_id; ?>"></span>
    <span id="listing_status" data-value="<?php echo $listing_status; ?>"></span>
    <span id="r_listing_low_home" data-value="<?php echo $r_listing_low_home; ?>"></span>
    <span id="r_listing_hi_home" data-value="<?php echo $r_listing_hi_home; ?>"></span>
    <span id="r_listing_other" data-value="<?php echo $r_listing_other; ?>"></span>
    <span id="r_listing_choose_low" data-value="<?php echo $r_listing_choose_low; ?>"></span>
    <span id="r_listing_choose_high" data-value="<?php echo $r_listing_choose_high; ?>"></span>
    <span id="wants_neigh" data-value="<?php echo $city_w; ?>"></span>
    <span id="neighbourhood_h" data-value="<?php echo $neighbourhood_h_js; ?>"></span>
    <span id="browser" data-name="<?php echo $browser['name']; ?>" data-version="<?php echo $browser['version']; ?>"></span>
    
</div>  