<?php

use \Model\Listing;


class Controller_Listing extends Controller_Template
{

	public function action_edit($listing_id = NULL)
	{
    
		$data["subnav"] = array('edit'=> 'active' );
    
    
    
		$this->template->title = 'Listing &raquo; Edit';
    
    $data['listing_id'] = $listing_id;
    $data['sub_type'] = 'sub type';
    $data['tut'] = true;
    
    $data['title'] = 'a';
    $data['imported'] = true;
    
    $data['req31'] = 'req31';
    $data['req29'] = 'req29';
    $data['req12'] = 'req12';
    $data['req13'] = 'req13';
    $data['req15'] = 'req15';
    $data['req16'] = 'req16';
    $data['req6'] = 'req6';
    $data['req17'] = 'req17';
    $data['req19'] = 'req19';
    $data['req20'] = 'req20';
    $data['req22'] = 'req22';
    $data['req8'] = 'req8';
    $data['req5'] = 'req5';
    $data['req21'] = 'req21';
    $data['req11'] = 'req11';
    $data['req9'] = 'req9';
    $data['req10'] = 'req10';
    $data['req24'] = 'req24';
    $data['req23'] = 'req23';
    $data['req10'] = 'req10';
    $data['req31'] = 'req31';
    $data['req21'] = 'req21';
    $data['req9'] = 'req9';
    $data['req11'] = 'req11';
    
    $data['creq6'] = 'creq6';
    $data['creq1'] = 'creq1';
    $data['creq2'] = 'creq2';
    $data['creq3'] = 'creq3';
    
    $data['listing_status'] = 'listing_status';
    $data['r_listing_low_home'] = 'r_listing_low_home';
    $data['r_listing_hi_home'] = 'r_listing_hi_home';
    $data['r_listing_other'] = 'r_listing_other';
    $data['r_listing_choose_low'] = 'r_listing_choose_low';
    $data['r_listing_choose_high'] = 'r_listing_choose_high';
    $data['city_w'] = 'true';
    $data['neighbourhood_h_js'] = 'true';
    $data['browser']['name'] = 'Firefox';
    $data['browser']['version'] = '40';
    
    $data['p_title_h']['field'] = 'p_title_h';
    $data['p_title_h']['value'] = 'a';
    $data['p_title_h']['options'] = Listing::p_title_h();
    $data['p_title_h']['attr'] = array(
      'id' => $data['p_title_h']['field'], 
      'class' => 'no_multi req ' . $data['req31']
    );
    
    $data['prop_style_h']['field'] = 'prop_style_h';
    $data['prop_style_h']['value'] = '';
    $data['prop_style_h']['options'] = Listing::prop_style_h();
    $data['prop_style_h']['attr'] = array(
      'id' => $data['prop_style_h']['field'], 
      'class' => 'no_multi req ' . $data['req11'],
      'style' => 'width: 250px;'
    );
    
    $data['p_siding_type_h']['field'] = 'siding_type_h[]';
    $data['p_siding_type_h']['value'] = array('a', 'b', 'c');
    $data['p_siding_type_h']['options'] = Listing::siding_type_h();
    $data['p_siding_type_h']['attr'] = array(
      'id' => 'siding_type_h',
      'class' => 'multi ' . $data['req21'],
      'multiple' => 'multiple'
    );
    
    $data['p_basement_h']['field'] = 'basement_h';
    $data['p_basement_h']['value'] = 'a';
    $data['p_basement_h']['options'] = Listing::basement_h();
    $data['p_basement_h']['attr'] = array(
      'id' => $data['p_basement_h']['field'],
      'class' => 'no_multi req ' . $data['req9']
    );
    
    
    $data['p_garage_h']['field'] = 'garage_h';
    $data['p_garage_h']['value'] = 'a';
    $data['p_garage_h']['options'] = Listing::garage_h();
    $data['p_garage_h']['attr'] = array(
      'id' => $data['p_garage_h']['field'],
      'class' => 'no_multi req ' . $data['req29']
    );
    
    
    $data['p_view_h']['field'] = 'view_h';
    $data['p_view_h']['value'] = 'a';
    $data['p_view_h']['options'] = Listing::view_h();
    $data['p_view_h']['attr'] = array(
      'id' => $data['p_view_h']['field'],
      'class' => 'no_multi ' . $data['req12']
    );
    
    
    $data['p_heating_source_h']['field'] = 'heating_source_h';
    $data['p_heating_source_h']['value'] = 'a';
    $data['p_heating_source_h']['options'] = Listing::heating_source_h();
    $data['p_heating_source_h']['attr'] = array(
      'id' => $data['p_heating_source_h']['field'],
      'class' => 'no_multi ' . $data['req13']
    );
    
    $data['req14'] = 'req14';
    $data['p_heating_h']['field'] = 'heating_h';
    $data['p_heating_h']['value'] = 'a';
    $data['p_heating_h']['options'] = Listing::heating_h();
    $data['p_heating_h']['attr'] = array(
      'id' => $data['p_heating_h']['field'],
      'class' => 'no_multi ' . $data['req14']
    );
    
    
    $data['p_cool_h']['field'] = 'cool_h';
    $data['p_cool_h']['value'] = 'a';
    $data['p_cool_h']['options'] = Listing::cool_h();
    $data['p_cool_h']['attr'] = array(
      'id' => $data['p_cool_h']['field'],
      'class' => 'no_multi ' . $data['req15']
    );
    
    
    $data['p_water_h']['field'] = 'water_h';
    $data['p_water_h']['value'] = 'a';
    $data['p_water_h']['options'] = Listing::water_h();
    $data['p_water_h']['attr'] = array(
      'id' => $data['p_water_h']['field'],
      'class' => 'no_multi ' . $data['req16']
    );
    
    
    $data['p_sewer_h']['field'] = 'sewer_h';
    $data['p_sewer_h']['value'] = 'a';
    $data['p_sewer_h']['options'] = Listing::sewer_h();
    $data['p_sewer_h']['attr'] = array(
      'id' => $data['p_sewer_h']['field'],
      'class' => 'no_multi ' . $data['req17']
    );
    
    
    $data['p_floor_type_h']['field'] = 'floor_type_h[]';
    $data['p_floor_type_h']['value'] = array('j', 'k', 'l');
    $data['p_floor_type_h']['options'] = Listing::floor_type_h();
    $data['p_floor_type_h']['attr'] = array(
      'id' => 'floor_type_h',
      'class' => 'multi ' . $data['req19'],
      'multiple' => 'multiple'
    );
    
    
    $data['p_roof_type_h']['field'] = 'roof_type_h[]';
    $data['p_roof_type_h']['value'] = array('d', 'e', 'f');
    $data['p_roof_type_h']['options'] = Listing::roof_type_h();
    $data['p_roof_type_h']['attr'] = array(
      'id' => 'roof_type_h',
      'class' => 'multi ' . $data['req20'],
      'multiple' => 'multiple'
    );
    
    
    $data['yr_built_h']['field'] = 'roof_type_h';
    $data['yr_built_h']['value'] = 1;
    $data['yr_built_h']['options'] = Listing::yr_built_h();
    $data['yr_built_h']['attr'] = array(
      'id' => $data['yr_built_h']['field'],
      'class' => 'no_multi ' . $data['req22']
    );
    
    //wants_info.php selects
    $prices = Listing::lprice_w();
    $data['lprice_l_w']['field'] = 'lprice_l_w';
    $data['lprice_l_w']['value'] = 300000;
    $data['lprice_l_w']['options'] = $prices;
    $data['lprice_l_w']['attr'] = array(
      'id' => $data['lprice_l_w']['field'],
      'class' => 'no_multi req ' . $data['req8']
    );
    
    $data['lprice_h_w']['field'] = 'lprice_h_w';
    $data['lprice_h_w']['value'] = 275000;
    $data['lprice_h_w']['options'] = $prices;
    $data['lprice_h_w']['attr'] = array(
      'id' => $data['lprice_h_w']['field'],
      'class' => 'no_multi req ' . $data['req8']
    );
    
    $data['sq_ft_w']['field'] = 'sq_ft_w';
    $data['sq_ft_w']['value'] = '3600|999999999'; //fix me
    $data['sq_ft_w']['options'] = Listing::sq_ft_w();
    $data['sq_ft_w']['attr'] = array(
      'id' => $data['sq_ft_w']['field'],
      'class' => 'no_multi req ' . $data['req17']
    );
    
    
    $data['bed_w']['field'] = 'bed_w';
    $data['bed_w']['value'] = '7';
    $data['bed_w']['options'] = Listing::bed_w();
    $data['bed_w']['attr'] = array(
      'id' => $data['bed_w']['field'],
      'class' => 'no_multi req ' . $data['req5']
    );
    
    
    $data['bath_w']['field'] = 'bath_w';
    $data['bath_w']['value'] = '6';
    $data['bath_w']['options'] = Listing::bath_w();
    $data['bath_w']['attr'] = array(
      'id' => $data['bath_w']['field'],
      'class' => 'no_multi req ' . $data['req6']
    );
    
    
    $data['p_title_w']['field'] = 'p_title_w[]';
    $data['p_title_w']['value'] = array(5, 3);
    $data['p_title_w']['options'] = Listing::p_title_w();
    $data['p_title_w']['attr'] = array(
      'id' => 'p_title_w',
      'class' => 'no_multi req ' . $data['req31']
    );
    
    $data['prop_style_w']['field'] = 'prop_style_w[]';
    $data['prop_style_w']['value'] = null;
    $data['prop_style_w']['options'] = array();
    $data['prop_style_w']['attr'] = array(
      'id' => 'prop_style_w',
      'class' => 'multi req',
      'multiple' => 'multiple',
      'style' => 'width: 210px;'
    );
    
    $data['t_lotsize_w']['field'] = 't_lotsize_w';
    $data['t_lotsize_w']['value'] = '1 AND 6000';
    $data['t_lotsize_w']['options'] = Listing::t_lotsize_w();
    $data['t_lotsize_w']['attr'] = array(
      'id' => $data['t_lotsize_w']['field'],
      'class' => 'no_multi'
    );
    
    $data['t_acreage_w']['field'] = 't_acreage_w';
    $data['t_acreage_w']['value'] = '6 AND 12';
    $data['t_acreage_w']['options'] = Listing::t_acreage_w();
    $data['t_acreage_w']['attr'] = array(
      'id' => $data['t_acreage_w']['field'],
      'class' => 'no_multi'
    );
    
    $data['siding_type_w']['field'] = 'siding_type_w[]';
    $data['siding_type_w']['value'] = array('k', 'f');
    $data['siding_type_w']['options'] = Listing::siding_type_w();
    $data['siding_type_w']['attr'] = array(
      'id' => 'siding_type_w',
      'class' => 'multi ' . $data['req21'],
      'multiple' => 'multiple'
    );
    
    $data['basement_w']['field'] = 'basement_w[]';
    $data['basement_w']['value'] = array('a', 'b');
    $data['basement_w']['options'] = Listing::basement_w();
    $data['basement_w']['attr'] = array(
      'id' => 'basement_w',
      'class' => 'no_multi ' . $data['req9']
    );
    
    $data['garage_w']['field'] = 'garage_w[]';
    $data['garage_w']['value'] = array('g', 'h');
    $data['garage_w']['options'] = Listing::garage_w();
    $data['garage_w']['attr'] = array(
      'id' => 'garage_w',
      'class' => 'multi',
      'multiple' => 'multiple'
    );
    
    $data['view_w']['field'] = 'view_w[]';
    $data['view_w']['value'] = array('a','b','c');
    $data['view_w']['options'] = Listing::view_w();
    $data['view_w']['attr'] = array(
      'id' => 'view_w',
      'class' => 'no_multi ' . $data['req12']
    );
    
    $data['heating_source_w']['field'] = 'heating_source_w[]';
    $data['heating_source_w']['value'] = 'a';
    $data['heating_source_w']['options'] = Listing::heating_source_w();
    $data['heating_source_w']['attr'] = array(
      'id' => 'heating_source_w',
      'class' => 'no_multi ' . $data['req13']
    );
    
    $data['heating_w']['field'] = 'heating_w[]';
    $data['heating_w']['value'] = 'e';
    $data['heating_w']['options'] = Listing::heating_w();
    $data['heating_w']['attr'] = array(
      'id' => 'heating_w',
      'class' => 'no_multi ' . $data['req14']
    );
    
    $data['cool_w']['field'] = 'cool_w[]';
    $data['cool_w']['value'] = 'c';
    $data['cool_w']['options'] = Listing::cool_w();
    $data['cool_w']['attr'] = array(
      'id' => 'cool_w',
      'class' => 'no_multi ' . $data['req15']
    );
    
    $data['water_w']['field'] = 'cool_w[]';
    $data['water_w']['value'] = 'd';
    $data['water_w']['options'] = Listing::water_w();
    $data['water_w']['attr'] = array(
      'id' => 'cool_w',
      'class' => $data['req16']
    );
    
    $data['sewer_w']['field'] = 'sewer_w[]';
    $data['sewer_w']['value'] = 'c';
    $data['sewer_w']['options'] = Listing::sewer_w();
    $data['sewer_w']['attr'] = array(
      'id' => 'sewer_w',
      'class' => 'no_multi ' . $data['req17']
    );
    
    $data['floor_type_w']['field'] = 'floor_type_w[]';
    $data['floor_type_w']['value'] = array('a','c','e');
    $data['floor_type_w']['options'] = Listing::floor_type_w();
    $data['floor_type_w']['attr'] = array(
      'id' => 'floor_type_w',
      'class' => 'multi ' . $data['req19'],
      'multiple' => 'multiple'
    );
    
    $data['roof_type_w']['field'] = 'roof_type_w[]';
    $data['roof_type_w']['value'] = array('b', 'd', 'h');
    $data['roof_type_w']['options'] = Listing::roof_type_w();
    $data['roof_type_w']['attr'] = array(
      'id' => 'roof_type_w',
      'class' => 'multi ' . $data['req20'],
      'multiple' => 'multiple'
    );
    
    $data['neighbourhood_h']['field'] = 'neighbourhood_h[]';
    $data['neighbourhood_h']['options'] = array();
    $data['neighbourhood_h']['value'] = '';
    $data['neighbourhood_h']['attr'] = array(
      'id' => 'neighbourhood_h',
      'class' => 'no_multi req',
      'title' => __('listing_neigh_title')
    );
    
    $data['storeys_h']['field'] = 'storeys_h';
    $data['storeys_h']['attr'] = array(
      'id' => $data['storeys_h']['field'],
      'class' => 'req10 no_multi req'
    );
    $imported = false;
    if($imported){
      $data['storeys_h']['attr']['disabled'] = 'disabled';
    }
    
    $data['storeys_h']['value'] = '';
    $data['storeys_h']['options'] = array();
    
    $data['storeys_w']['field'] = 'storeys_w[]';
    $data['storeys_w']['options'] = array();
    $data['storeys_w']['value'] = null;
    $data['storeys_w']['attr'] = array(
      'id' => 'storeys_w',
      'class' => 'no_multi req ' . $data['req10']
    );
    
    $data['neighbourhood_w']['field'] = 'neighbourhood_w[]';
    $data['neighbourhood_w']['options'] = array();
    $data['neighbourhood_w']['value'] = null;
    $data['neighbourhood_w']['attr'] = array(
      'class' => 'req',
      'multiple' => 'multiple',
      'title' => __('listing_neigh_title'),
      'disabled' => 'disabled',
      'style' => 'min-width:220px;'
    );
    
    
    //checkboxes part
    $check_value = 1;
    $check_class = 'customized-checkbox';
    
    $listing_confeat_values = Listing::checkbox_confeat_values();
    $data['c_confeat'] = $this->checkbox_id_mapping($listing_confeat_values, $check_class);
    $data['c_confeat']['value'] = $check_value;
    
    //checked status modifications example:
    $data['c_confeat']['electric_h']['checked'] = true;
    
    $strata_strinct_h = Listing::checkbox_strata_strict_h();
    $data['strata_strict_h'] = $this->checkbox_id_mapping($strata_strinct_h, $check_class);
    $data['strata_strict_h']['value'] = $check_value;
    
    $listing_int_feat_h = Listing::checkbox_int_feat_h();
    $data['int_feat_h'] = $this->checkbox_id_mapping($listing_int_feat_h, $check_class);
    $data['int_feat_h']['value'] = $check_value;
    
    $listing_kitchen_h = Listing::checkbox_kitchen_h();
    $data['kitchen_h'] = $this->checkbox_id_mapping($listing_kitchen_h, $check_class);
    $data['kitchen_h']['value'] = $check_value;
    
    $listing_laundry_h = Listing::checkbox_laundry_h();
    $data['laundry_h'] = $this->checkbox_id_mapping($listing_laundry_h, $check_class);
    $data['laundry_h']['value'] = $check_value;
    
    $listing_prop_feat_h = Listing::checkbox_prop_feat_h();
    $data['prop_feat_h'] = $this->checkbox_id_mapping($listing_prop_feat_h, $check_class);
    $data['prop_feat_h']['value'] = $check_value;
    
    $listing_neigh_feat_h = Listing::checkbox_neigh_feat_h();
    $data['neigh_feat_h'] = $this->checkbox_id_mapping($listing_neigh_feat_h, $check_class);
    $data['neigh_feat_h']['value'] = $check_value;
    
    
    
    //input type text part
    $data['fname_c']['field'] = 'fname_c';
    $data['fname_c']['value'] = '';
    
    $data['lname_c']['field'] = 'lname_c';
    $data['lname_c']['value'] = '';
    
    $data['client_email_c']['field'] = 'client_email_c';
    $data['client_email_c']['value'] = '';
    
    $data['pnumber_c']['field'] = 'pnumber_c';
    $data['pnumber_c']['value'] = '';
    
    $cnumber_c_val = 0;
    $data['cnumber_c']['field'] = 'cnumber_c';
    $data['cnumber_c']['value'] = ($cnumber_c_val != 0) ? $cnumber_c_val : '';
    
    $data['has_paddress']['field'] = 'has_paddress';
    $data['has_paddress']['value'] = '';
    $data['has_paddress']['disabled'] = ($data['imported']) ? 'disabled="disabled"' : '';
    
    $data['has_address']['field'] = 'has_address';
    $data['has_address']['value'] = '';
    $data['has_address']['disabled'] = ($data['imported']) ? 'disabled="disabled"' : '';
    
    $data['z_pcode_h']['field'] = 'z_pcode_h';
    $data['z_pcode_h']['value'] = '';
    $data['z_pcode_h']['disabled'] = ($data['imported']) ? 'disabled="disabled"' : '';
    
    $lprice_h_val = 100;
    $data['lprice_h']['field'] = 'lprice_h';
    $data['lprice_h']['value'] = ($lprice_h_val) ? number_format($lprice_h_val, 0) : '';
    
    $sq_ft_h_val = 1000;
    $data['sq_ft_h']['field'] = 'sq_ft_h';
    $data['sq_ft_h']['value'] = ($sq_ft_h_val) ? number_format($sq_ft_h_val, 0) : '';
    
    $data['bed_h']['field'] = 'bed_h';
    $data['bed_h']['value'] = '';
    
    $data['bath_h']['field'] = 'bath_h';
    $data['bath_h']['value'] = '';
    
    $lot_size_h_val = 100;
    $data['lot_size_h']['field'] = 'lot_size_h';
    $data['lot_size_h']['value'] = ($lot_size_h_val) ? number_format($lot_size_h_val, 0) : '';
    
    $t_acreage_h_val = 10000;
    $data['t_acreage_h']['field'] = 't_acreage_h';
    $data['t_acreage_h']['value'] = ($t_acreage_h_val) ? number_format($t_acreage_h_val, 0) : '';
    
    $prop_tax_h_val = 50;
    $data['prop_tax_h']['field'] = 'prop_tax_h';
    $data['prop_tax_h']['value'] = ($prop_tax_h_val) ? number_format($prop_tax_h_val, 0) : '';
    
    $strata_fees_h_val = 60;
    $data['strata_fees_h']['field'] = 'strata_fees_h';
    $data['strata_fees_h']['value'] = ($strata_fees_h_val) ? number_format($strata_fees_h_val, 2) : '';
    
    $school_h_val = '1';
    $data['school_h']['field'] = 'school_h';
    $data['school_h']['value'] = ($school_h_val != '0') ? $school_h_val : '';
    
    $yt_val = 3;
    $data['yt']['field'] = 'yt';
    $data['yt']['value'] = ($yt_val != 0) ? $yt_val : '';
    $data['yt']['disabled'] = ($data['imported']) ? 'disabled="disabled"' : '';
    
    $vtl_val = 2;
    $data['vtl']['field'] = 'vtl';
    $data['vtl']['value'] = ($vtl_val != 0) ? $vtl_val : '';
    $data['vtl']['disabled'] = ($data['imported']) ? 'disabled="disabled"' : '';
    
    $data['wants_address']['field'] = 'wants_address';
    $data['wants_address']['value'] = '';
    
    //partials and dummy data
    $data['ml_num'] = 1;
    $data['forgot_email'] = 'forgot_email';
    
    $data['r_listing_fn'] = 'r_listing_fn';
    $data['r_listing_ln'] = 'r_listing_ln';
    $data['r_listing_pn'] = 'r_listing_pn';
    $data['r_listing_cn'] = 'r_listing_cn';
    $data['r_listing_water_type'] = 'r_listing_water_type';
    $data['r_listing_sewer_type'] = 'r_listing_sewer_type';
    $data['r_listing_wants_req'] = 'r_listing_wants_req';
    $data['r_listing_neigh'] = 'r_listing_neigh';
    $data['r_listing_zip'] = 'r_listing_zip';
    $data['r_listing_price'] = 'r_listing_price';
    $data['r_listing_sqfeet'] = 'r_listing_sqfeet';
    $data['r_listing_property'] = 'r_listing_property';
    $data['r_listing_bed'] = 'r_listing_bed';
    $data['r_listing_bath'] = 'r_listing_bath';
    $data['r_listing_basement'] = 'r_listing_basement';
    $data['r_listing_garage'] = 'r_listing_garage';
    $data['r_listing_view'] = 'r_listing_view';
    $data['r_listing_ybuilt'] = 'r_listing_ybuilt';
    $data['r_listing_wants_price'] = 'r_listing_wants_price';
    $data['r_listing_beds'] = 'r_listing_beds';
    $data['r_listing_baths'] = 'r_listing_baths';
    $data['r_listing_title'] = 'r_listing_title';
    $data['r_listing_storeys'] = 'r_listing_storeys';
    $data['r_listing_basement_type'] = 'r_listing_basement_type';
    $data['r_listing_sqf'] = 1000;
    $data['r_listing_title_stitle'] = 'r_listing_title_stitle';
    $data['r_listing_title_lease'] = 'r_listing_title_lease';
    $data['r_listing_title_coop'] = 'r_listing_title_coop';
    $data['r_listing_title_fee'] = 'r_listing_title_fee';
    $data['r_listing_title_freehold'] = 'r_listing_title_freehold';
    $data['r_listing_lotsize'] = 'r_listing_lotsize';
    $data['r_listing_acreage'] = 'r_listing_acreage';
    $data['r_listing_siding_type'] = 'r_listing_siding_type';
    $data['r_listing_siding_brick'] = 'r_listing_siding_brick';
    $data['r_listing_siding_cement'] = 'r_listing_siding_cement';
    $data['r_listing_siding_compo'] = 'r_listing_siding_compo';
    $data['r_listing_siding_metal'] = 'r_listing_siding_metal';
    $data['r_listing_siding_shingle'] = 'r_listing_siding_shingle';
    $data['r_listing_siding_stone'] = 'r_listing_siding_stone';
    $data['r_listing_siding_stucco'] = 'r_listing_siding_stucco';
    $data['r_listing_siding_vinyl'] = 'r_listing_siding_vinyl';
    $data['r_listing_siding_wood'] = 'r_listing_siding_wood';
    $data['r_listing_floor_concrete'] = 'r_listing_floor_concrete';
    $data['r_listing_siding_mixed'] = 'r_listing_siding_mixed';
    $data['r_listing_other'] = 'r_listing_other';
    $data['r_listing_sbasement'] = 'r_listing_sbasement';
    $data['r_listing_base_n'] = 'r_listing_base_n';
    $data['r_listing_base_unfin'] = 'r_listing_base_unfin';
    $data['r_listing_base_pfinish'] = 'r_listing_base_pfinish';
    $data['r_listing_base_finish_base'] = 'r_listing_base_finish_base';
    $data['r_listing_heat_source'] = 'r_listing_heat_source';
    $data['r_listing_heat_type'] = 'r_listing_heat_type';
    $data['r_listing_cool_type'] = 'r_listing_cool_type';
    $data['r_listing_floor_type'] = 'r_listing_floor_type';
    $data['r_listing_roof_type'] = 'r_listing_roof_type';
    $data['r_listing_ptax'] = 'r_listing_ptax';
    $data['r_listing_has_strata'] = 'r_listing_has_strata';
    $data['r_listing_lsd'] = 'r_listing_lsd';
    $data['r_listing_has_strata_inc'] = 'r_listing_has_strata_inc';
    $data['r_listing_has_strata_rest'] = 'r_listing_has_strata_rest';
    $data['r_listing_ifeature'] = 'r_listing_ifeature';
    $data['r_listing_kfeature'] = 'r_listing_kfeature';
    $data['r_listing_lfeatures'] = 'r_listing_lfeatures';
    $data['r_listing_pfeature'] = 'r_listing_pfeature';
    $data['r_listing_nfeature'] = 'r_listing_nfeature';
    $data['r_listing_has_add_comments'] = 'r_listing_has_add_comments';
    
    $data['cnumber_c'] = 1;
    $data['new_wants'] = 1;
    $data['type_of_listing'] = 2;
    $data['title_button'] = 'title button';
    $data['attention_type_h'] = 'attention type';
    $data['new_has'] = 'new_has';
    $data['new_images'] = 'new_images';
    $data['matches'] = 'matches';
    $data['lprice_h'] = 1000;
    $data['sq_ft_h'] = 1000;
    $data['lot_size_h'] = 1000;
    $data['t_acreage_h'] = 1000;
    $data['r_listing_has_acres'] = 1000;
    $data['siding_type_h'] = array(1,2,3);
    $data['search_and'] = '----';
    $data['search_between'] = '----';
    $data['attention_type_c'] = 'attention_type_c';
    $data['new_client'] = false;
    $data['prop_tax_h'] = 100;
    $data['strata_fees'] = '0';
    $data['cpc_address'] = 'cpc_address';
    $data['if_exists'][0] = 0;
    $data['attention_type_w'] = 'attention_type_w';
    $data['comments_h'] = 'comments_h';
    
    $data['client_info'] = View::forge('listing/client_info', $data);
    $data['has_info'] = View::forge('listing/client_has_info', $data);
    $data['image_info'] = View::forge('listing/image_info', $data);
    $data['wants_info'] = View::forge('listing/wants_info', $data);
    
    $this->template->content = View::forge('listing/edit', $data);
    
    
	}

	public function action_update()
	{
		$data["subnav"] = array('update'=> 'active' );
		$this->template->title = 'Listing &raquo; Update';
		$this->template->content = View::forge('listing/update', $data);
	}
  
  /**
   * options parameter generation : mapping id because id has different value with name field
   */
  private function checkbox_id_mapping($listing, $class_name)
  {
    
    $return = array();
    
    foreach($listing as $key => $vals)
    {
      $return[$key] = $vals;
      $return[$key]['options'] = array(
        'class' => $class_name,
        'id' => $key
      );
    }
    
    return $return;
  }

}
