<?php


namespace Model;

class Listing extends \Model {
  
  /** FIX ME add __() for locale */
  
  /** select dropdown model */
  public static function p_title_h()
  {
    
    return array(
      __('listing_title_stitle'),
      'a' => __('listing_title_lease'),
      'b' => __('listing_title_coop'),
      'c' => __('listing_title_fee'),
      'd' => __('listing_title_freehold')
    );
    
  }
  
  public static function prop_style_h()
  {
    return array();
  }
  
  public static function siding_type_h()
  {
    
    return array(
      'a' => __('listing_siding_brick'),
      'b' => __('listing_siding_cement'),
      'c' => __('listing_siding_compo'),
      'd' => __('listing_siding_metal'),
      'e' => __('listing_siding_shingle'),
      'f' => __('listing_siding_stone'),
      'g' => __('listing_siding_stucco'),
      'h' => __('listing_siding_vinyl'),
      'i' => __('listing_siding_wood'),
      'j' => __('listing_siding_mixed'),
      'k' => __('listing_other'),
      'l' => __('listing_floor_concrete')
    );
    
  }
  
  public static function basement_h()
  {
    return array(
      __('listing_sbasement'),
      'a' => __('listing_base_n'),
      'b' => __('listing_base_unfin'),
      'c' => __('listing_base_pfinish'),
      'd' => __('listing_base_finish_base')
    );
  }
  
  public static function garage_h()
  {
    return array(
      __('listing_garage'),
      'a' => __('listing_garage_ng'),
      'b' => __('listing_garage_sbg'),
      'c' => __('listing_garage_dbg'),
      'd' => __('listing_garage_tbg'),
      'e' => __('listing_garage_dp'),
      'f' => __('listing_garage_sp'),
      'g' => __('listing_garage_up'),
      'h' => __('listing_garage_cp'),
      'i' => __('listing_other')
    );
    
  }
  
  public static function view_h()
  {
    return array(
      __('listing_sview'),
      'a' => __('listing_none'),
      'b' => __('listing_view_city'),
      'c' => __('listing_view_mountain'),
      'd' => __('listing_view_ocean'),
      'e' => __('listing_view_lake'),
      'f' => __('listing_view_river'),
      'g' => __('listing_view_golf'),
      'h' => __('listing_view_harbour')
    );
  }
  
  public static function heating_source_h()
  {
    return array(
      __('listing_sheat_source'),
      'a' => __('listing_heats_ngas'),
      'b' => __('listing_heats_electric'),
      'c' => __('listing_heats_propane'),
      'd' => __('listing_heats_oil'),
      'e' => __('listing_heats_wood'),
      'f' => __('listing_heats_solar'),
      'g' => __('listing_heats_coal'),
      'h' => __('listing_heats_water'),
      'i' => __('listing_other')
    );
  }
  
  public static function heating_h()
  {
    return array(
      __('listing_sheat_type'),
      'a' => __('listing_heat_cheating'),
      'b' => __('listing_heat_bheating'),
      'c' => __('listing_heat_geo'),
      'd' => __('listing_heat_floor'),
      'e' => __('listing_other')
    );
  }
  
  public static function cool_h()
  {
    return array(
      __('listing_scool_type'),
      'a' => __('listing_none'),
      'b' => __('listing_cool_ac'),
      'c' => __('listing_heat_geo'),
      'd' => __('listing_other')
    );
  }
  
  public static function water_h()
  {
    return array(
      __('listing_swater_type'),
      'a' => __('listing_water_type_cwater'),
      'b' => __('listing_water_type_well'),
      'c' => __('listing_water_type_cystern'),
      'd' => __('listing_other')
    );
  }
  
  public static function sewer_h()
  {
    return array(
      __('listing_ssewer_type'),
      'a' => __('listing_sewer_type_city'),
      'b' => __('listing_sewer_type_septic'),
      'c' => __('listing_sewer_type_cystern'),
      'd' => __('listing_other')
    );
  }
  
  public static function floor_type_h()
  {
    return array(
      'a' => __('listing_floor_carpet'),
      'b' => __('listing_floor_concrete'),
      'c' => __('listing_floor_hardwood'),
      'd' => __('listing_floor_laminate'),
      'e' => __('listing_floor_lino'),
      'f' => __('listing_floor_slate'),
      'g' => __('listing_floor_tile'),
      'h' => __('listing_floor_stone'),
      'i' => __('listing_floor_mixed'),
      'j' => __('listing_other')
    );
  }
  
  public static function roof_type_h()
  {
    return array(
      'a' => __('listing_roof_ashphalt'),
      'b' => __('listing_roof_buildup'),
      'c' => __('listing_roof_compo'),
      'd' => __('listing_roof_metal'),
      'e' => __('listing_roof_shake'),
      'f' => __('listing_roof_slate'),
      'g' => __('listing_roof_tile'),
      'h' => __('listing_other')
    );
  }
  
  public static function yr_built_h()
  {
    return array(
      __('listing_has_year'),
      'a' => __('listing_has_new'),
      'b' => __('listing_has_old')
    );
  }
  
  public static function lprice_w()
  {
    $amounts = array(
      __('listing_wants_sprice'),
      
      '1', '25000', '50000', '75000', '100000', '125000', '150000', '175000', '200000', '225000',
      
      '250000', '275000', '300000', '325000', '350000', '375000', '400000', '425000', '450000', '475000',
      
      '500000', '550000', '600000', '650000', '700000', '750000', '800000', '850000', '900000', '950000',
      
      '1000000','1250000','1500000','1750000','2000000','2250000','2500000','2750000', '3000000', '3250000',
      
      '3500000','3750000','4000000','4250000','4500000','4750000','5000000','5500000','6000000','6500000',
      
      '7000000','7500000','8000000','8500000','9500000','9000000','10000000','12000000','15000000','20000000',
      
      '25000000','30000000','35000000','40000000','45000000','50000000','55000000','60000000','65000000','70000000',

      '75000000','80000000','85000000','90000000','95000000','100000000'
    );
    
    $return = array();
    foreach($amounts as $k => $amount)
    { 
      if($k > 0)
      {
        $return[$amount] = '$' . number_format($amount);
      }
      else
      {
        $return[0] = $amount;
      }
    }
    
    return $return;
  }
  
  public static function sq_ft_w()
  {
    
    $key = __('listing_sqf');
    
    return array(
      __('listing_wants_ssqft'),
      '1|600' => 'Any ' . $key,
      '600|1200' => '600+ ' . $key,
      '1200|2400' => '1200+ ' . $key,
      '2400|3600' => '2400+ ' . $key,
      '3600|4800' => '3600+ ' . $key,
      '4800|5600' => '4800+ ' . $key,
      '5600|7400' => '5600+ ' . $key,
      '7400|10000' => '7400+ ' . $key,
      '10000|12000' => '10,000+ ' . $key,
      '12000|14000' => '12,000+ ' . $key,
      '14000|16000' => '14,000+ ' . $key,
      '16000|18000' => '16,000+ ' . $key,
      '18000|20000' => '18,000+ ' . $key
    );
  }
  
  public static function bed_w()
  {
    
    $key = __('listing_beds');
    return array(
      '0' => __('listing_select_amounts'),
      '1' => '1+ ' . $key, 
      '2' => '2+ ' . $key, 
      '3' => '3+ ' . $key,
      '4' => '4+ ' . $key, 
      '5' => '5+ ' . $key, 
      '6' => '6+ ' . $key, 
      '7' => '7+ ' . $key
    );
  }
  
  public static function bath_w()
  {
    
    $key = __('listing_baths');
    return array(
      '0' => __('listing_select_amounts'),
      '1' => '1+ ' . $key, 
      '2' => '2+ ' . $key, 
      '3' => '3+ ' . $key, 
      '4' => '4+ ' . $key, 
      '5' => '5+ ' . $key, 
      '6' => '6+ ' . $key, 
      '7' => '7+ ' . $key
    );
  }
  
  public static function p_title_w()
  {
    return array(
      __('listing_title_stitle'),
      'a' => __('listing_title_lease'),
      'b' => __('listing_title_coop'),
      'c' => __('listing_title_fee'),
      'd' => __('listing_title_freehold')
    );
  }
  
  public static function prop_style_w()
  {
    
  }
  
  public static function t_lotsize_w()
  {
    $key = __('listing_sqf');
    return array(
      __('listing_wants_slot'),
      '1 AND 6000' => '0-6000 ' . $key,
      '6000 AND 10000' => '6000-10,000 ' . $key,
      '10000 AND 2000000' => '10,000+ ' . $key,
      '9999999' => 'listing_any'
    );
  }
  
  public static function t_acreage_w()
  {
    return array(
      __('listing_wants_sacreage'),
      '1 AND 2' => '1-2',
      '2 AND 6' => '2-6',
      '6 AND 12' => '6-12',
      '12 AND 30' => '12-30',
      '30 AND 60' => '30-60',
      '60 AND 20000' => '60+',
      '9999999' => __('listing_any')
    );
  }
  
  public static function siding_type_w()
  {
    return array(
      '9' => __('listing_any'),
      'a' => __('listing_siding_brick'),
      'b' => __('listing_siding_cement'),
      'c' => __('listing_siding_compo'),
      'd' => __('listing_siding_metal'),
      'e' => __('listing_siding_shingle'),
      'f' => __('listing_siding_stone'),
      'g' => __('listing_siding_stucco'),
      'h' => __('listing_siding_vinyl'),
      'i' => __('listing_siding_wood'),
      'j' => __('listing_siding_mixed'),
      'k' => __('listing_other'),
      'l' => __('listing_floor_concrete')
    );
  }
  
  public static function basement_w()
  {
    return array(
      '9' => __('listing_any'),
      'a' => __('listing_base_n'),
      'b' => __('listing_base_unfin'),
      'c' => __('listing_base_pfinish'),
      'd' => __('listing_base_finish_base')
    );
  }
  
  public static function garage_w()
  {
    return array(
      '9' => __('listing_any'),
      'a' => __('listing_garage_ng'),
      'b' => __('listing_garage_sbg'),
      'c' => __('listing_garage_dbg'),
      'd' => __('listing_garage_tbg'),
      'e' => __('listing_garage_dp'),
      'f' => __('listing_garage_sp'),
      'g' => __('listing_garage_up'),
      'h' => __('listing_garage_cp'),
      'i' => __('listing_other')
    );
  }
  
  public static function view_w()
  {
    return array(
      '9' => __('listing_any'),
      'a' => __('listing_none'),
      'b' => __('listing_view_city'),
      'c' => __('listing_view_mountain'),
      'd' => __('listing_view_ocean'),
      'e' => __('listing_view_lake'),
      'f' => __('listing_view_river'),
      'g' => __('listing_view_golf'),
      'h' => __('listing_view_harbour')
    );
  }
  
  public static function heating_source_w()
  {
    return array(
      '9' => __('listing_any'),
      'a' => __('listing_heats_ngas'),
      'b' => __('listing_heats_electric'),
      'c' => __('listing_heats_propane'),
      'd' => __('listing_heats_oil'),
      'e' => __('listing_heats_wood'),
      'f' => __('listing_heats_solar'),
      'g' => __('listing_heats_coal'),
      'h' => __('listing_heats_water')
    );
  }
  
  public static function heating_w()
  {
    return array(
      '9' => __('listing_any'),
      'a' => __('listing_heat_cheating'),
      'b' => __('listing_heat_bheating'),
      'c' => __('listing_heat_geo'),
      'd' => __('listing_heat_floor'),
      'e' => __('listing_other')
    );
  }
  
  public static function cool_w()
  {
    return array(
      '9' => __('listing_any'),
      'a' => __('listing_none'),
      'b' => __('listing_cool_ac'),
      'c' => __('listing_heat_geo'),
      'd' => __('listing_other')
    );
  }
  
  public static function water_w()
  {
    return array(
      '9' => __('listing_any'),
      'a' => __('listing_water_type_cwater'),
      'b' => __('listing_water_type_well'),
      'c' => __('listing_water_type_cystern'),
      'd' => __('listing_other')
    );
  }
  
  public static function sewer_w()
  {
    return array(
      '9' => __('listing_any'),
      'a' => __('listing_sewer_type_city'),
      'b' => __('listing_sewer_type_septic'),
      'c' => __('listing_sewer_type_cystern'),
      'd' => __('listing_other')
    );
  }
  
  public static function floor_type_w()
  {
    return array(
      '9' => __('listing_any'),
      'a' => __('listing_floor_carpet'),
      'b' => __('listing_floor_concrete'),
      'c' => __('listing_floor_hardwood'),
      'd' => __('listing_floor_laminate'),
      'e' => __('listing_floor_lino'),
      'f' => __('listing_floor_slate'),
      'g' => __('listing_floor_tile'),
      'h' => __('listing_floor_stone'),
      'i' => __('listing_floor_mixed'),
      'j' => __('listing_other')
    );
  }
  
  public static function roof_type_w()
  {
    return array(
      '9' => __('listing_any'),
      'a' => __('listing_roof_ashphalt'),
      'b' => __('listing_roof_buildup'),
      'c' => __('listing_roof_compo'),
      'd' => __('listing_roof_metal'),
      'e' => __('listing_roof_shake'),
      'f' => __('listing_roof_slate'),
      'g' => __('listing_roof_tile'),
      'h' => __('listing_other')
    );
  }
  
  
  /** input type checkboxes data */
  
  /** 
   * checkbox data model format
   *    id of checkbox field contain array with index 'name' and 'checked'
   */
  public static function checkbox_confeat_values()
  {
    return array(
      'cable_h' => array(
        'label' => __('listing_has_strata_inc_cable'),
        'field' => 'confeat1_h',
        'checked' => false 
      ),
      'caretaker_h' => array(
        'label' => __('listing_has_strata_inc_care'),
        'field' => 'confeat2_h',
        'checked' => false
      ),
      'garbage_pickup_h' => array(
        'label' => __('listing_has_strata_inc_garbage'),
        'field' => 'confeat3_h',
        'checked' => false
      ),
      'heat_h' => array(
        'label' => __('listing_has_strata_inc_heat'),
        'field' => 'confeat4_h',
        'checked' => false
      ),
      'hot_water_h' => array(
        'label' => __('listing_has_strata_inc_hot'),
        'field' => 'confeat5_h',
        'checked' => false
      ),
      'management_h' => array(
        'label' => __('listing_has_strata_inc_manage'),
        'field' => 'confeat6_h',
        'checked' => false
      ),
      'snow_removal_h' => array(
        'label' => __('listing_has_strata_inc_snow'),
        'field' => 'confeat7_h',
        'checked' => false
      ),
      'other6_h' => array(
        'label' => __('listing_has_strata_inc_water'),
        'field' => 'confeat8_h',
        'checked' => false
      ),
      'water_inc_h' => array(
        'label' => __('listing_has_strata_inc_electric'),
        'field' => 'confeat9_h',
        'checked' => false
      ),
      'electric_h' => array(
        'label' => __('listing_other'),
        'field' => 'confeat10_h',
        'checked' => false
      )
    );
  }
  
  public static function checkbox_strata_strict_h()
  {
    return array(
      'age_strict_h' => array(
        'label' => __('listing_has_strata_rest_age'),
        'field' => 'confeat11_h',
        'checked' => false
      ),
      'no_pets_allowed_h' => array(
        'label' => __('listing_has_strata_rest_no_pets'),
        'field' => 'confeat12_h',
        'checked' => false
      ),
      'any_pets_allowed_h' => array(
        'label' => __('listing_has_strata_rest_pets'),
        'field' => 'confeat13_h',
        'checked' => false
      ),
      'pets_allowed_strict_h' => array(
        'label' => __('listing_has_strata_rest_pets_allowed'),
        'field' => 'confeat14_h',
        'checked' => false
      ),
      'is_not_rentable_h' => array(
        'label' => __('listing_has_strata_rest_no_rent'),
        'field' => 'confeat15_h',
        'checked' => false
      ),
      'is_rentable_h' => array(
        'label' => __('listing_has_strata_rest_rent_allowed'),
        'field' => 'confeat16_h',
        'checked' => false 
      ),
      'is_rentable_strict_h' => array(
        'label' => __('listing_has_strata_rest_rent_rest'),
        'field' => 'confeat17_h',
        'checked' => false 
      ),
      'no_strict_h' => array(
        'label' => __('listing_has_strata_rest_no_rest'),
        'field' => 'confeat18_h',
        'checked' => false 
      ),
    );
  }
  
  public static function checkbox_int_feat_h()
  {
    return array(
      'fireplace_h' => array(
        'label' => __('listing_ifeature_fire'),
        'field' => 'intfeat1_h',
        'checked' => false
      ),
      'ceiling_fans_h' => array(
        'label' => __('listing_ifeature_fans'),
        'field' => 'intfeat2_h',
        'checked' => false
      ),
      'attic_h' => array(
        'label' => __('listing_ifeature_attic'),
        'field' => 'intfeat3_h',
        'checked' => false
      ),
      'intercom_h' => array(
        'label' => __('listing_ifeature_intercom'),
        'field' => 'intfeat4_h',
        'checked' => false
      ),
      'jetted_tub_h' => array(
        'label' => __('listing_ifeature_tub'),
        'field' => 'intfeat5_h',
        'checked' => false
      ),
      'security_system_h' => array(
        'label' => __('listing_ifeature_ssystem'),
        'field' => 'intfeat6_h',
        'checked' => false
      ),
      'skylights_h' => array(
        'label' => __('listing_ifeature_sky'),
        'field' => 'intfeat7_h',
        'checked' => false
      ),
      'vaulted_ceiling_h' => array(
        'label' => __('listing_ifeature_vceiling'),
        'field' => 'intfeat8_h',
        'checked' => false
      ),
      'other1_h' => array(
        'label' => __('listing_other'),
        'field' => 'intfeat9_h',
        'checked' => false
      )
    );
  }
  
  public static function checkbox_kitchen_h()
  {
    return array(
      'fridge_h' => array(
        'label' => __('listing_kfeature_fridge'),
        'field' => 'kitchen1_h',
        'checked' => false
      ),
      'stove_h' => array(
        'label' => __('listing_kfeature_stove'),
        'field' => 'kitchen2_h',
        'checked' => false
      ),
      'built_in_oven_h' => array(
        'label' => __('listing_kfeature_built'),
        'field' => 'kitchen3_h',
        'checked' => false
      ),
      'microwave_h' => array(
        'label' => __('listing_kfeature_microwave'),
        'field' => 'kitchen4_h',
        'checked' => false
      ),
      'Dishwasher_h' => array(
        'label' => __('listing_kfeature_dish'),
        'field' => 'kitchen5_h',
        'checked' => false
      ),
      'Garburator_h' => array(
        'label' => __('listing_kfeature_garb'),
        'field' => 'kitchen6_h',
        'checked' => false
      ),
      'Trash_Compactor_h' => array(
        'label' => __('listing_kfeature_trash'),
        'field' => 'kitchen7_h',
        'checked' => false
      ),
      'other2_h' => array(
        'label' => __('listing_other'),
        'field' => 'kitchen8_h',
        'checked' => false
      ),
    );
  }
  
  public static function checkbox_laundry_h()
  {
    return array(
      'Washer_h' => array(
        'label' => __('listing_lfeatures_washer'),
        'field' => 'laundry1_h',
        'checked' => false
      ),
      'Dryer_h' => array(
        'label' => __('listing_lfeatures_dryer'),
        'field' => 'laundry2_h',
        'checked' => false
      ),
      'Washer_Dryer_Combo_h' => array(
        'label' => __('listing_lfeatures_combo'),
        'field' => 'laundry3_h',
        'checked' => false
      ),
      'Laundry_Sink_h' => array(
        'label' => __('listing_lfeatures_sink'),
        'field' => 'laundry4_h',
        'checked' => false
      ),
      'other3_h' => array(
        'label' => __('listing_other'),
        'field' => 'laundry5_h',
        'checked' => false
      )
    );
  }
  
  public static function checkbox_prop_feat_h()
  {
    return array(
      'pool_h' => array(
        'label' => __('listing_pfeature_pool'),
        'field' => 'propfeat1_h',
        'checked' => false
      ),
      'acreage_h' => array(
        'label' => __('listing_pfeature_acreage'),
        'field' => 'propfeat2_h',
        'checked' => false
      ),
      'work_shop_h' => array(
        'label' => __('listing_pfeature_shop'),
        'field' => 'propfeat3_h',
        'checked' => false
      ),
      'deck_h' => array(
        'label' => __('listing_pfeature_deck'),
        'field' => 'propfeat4_h',
        'checked' => false
      ),
      'dock_h' => array(
        'label' => __('listing_pfeature_dock'),
        'field' => 'propfeat5_h',
        'checked' => false
      ),
      'greenhouse_h' => array(
        'label' => __('listing_pfeature_green'),
        'field' => 'propfeat6_h',
        'checked' => false
      ),
      'hot_tub_spa_h' => array(
        'label' => __('listing_pfeature_spa'),
        'field' => 'propfeat7_h',
        'checked' => false
      ),
      'sauna_h' => array(
        'label' => __('listing_pfeature_sauna'),
        'field' => 'propfeat8_h',
        'checked' => false
      ),
      'pond_h' => array(
        'label' => __('listing_pfeature_pond'),
        'field' => 'propfeat9_h',
        'checked' => false
      ),
      'sprinkler_system_h' => array(
        'label' => __('listing_pfeature_sprinkler'),
        'field' => 'propfeat10_h',
        'checked' => false
      ),
      'water_front_h' => array(
        'label' => __('listing_pfeature_water'),
        'field' => 'propfeat11_h',
        'checked' => false
      ),
      'patio_h' => array(
        'label' => __('listing_pfeature_patio'),
        'field' => 'propfeat12_h',
        'checked' => false
      ),
      'other4_h' => array(
        'label' => __('listing_ofeature'),
        'field' => 'propfeat13_h',
        'checked' => false
      )
    );
  }
  
  public static function checkbox_neigh_feat_h()
  {
    return array(
      'community_pool_h' => array(
        'label' => __('listing_nfeature_compool'),
        'field' => 'neighfeat1_h',
        'checked' => false
      ),
      'gated_community_h' => array(
        'label' => __('listing_nfeature_gate'),
        'field' => 'neighfeat2_h',
        'checked' => false
      ),
      'golf_course_location_h' => array(
        'label' => __('listing_nfeature_golf'),
        'field' => 'neighfeat3_h',
        'checked' => false
      ),
      'clubhouse_h' => array(
        'label' => __('listing_nfeature_club'),
        'field' => 'neighfeat4_h',
        'checked' => false
      ),
      'water_front1_h' => array(
        'label' => __('listing_nfeature_water'),
        'field' => 'neighfeat5_h',
        'checked' => false
      ),
      'other5_h' => array(
        'label' => __('listing_ofeature'),
        'field' => 'neighfeat5_h',
        'checked' => false
      )
    );
  }
  
  
  
}